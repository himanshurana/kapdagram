const mongoose = require("mongoose");
const db = require("./");

const variantSchema = mongoose.Schema({
  // sku: { type: String, required: true },
  assests: {
    imgs: [{ type: String, required: true }]
  },
  attributes: {
    color: { type: String, required: true },
    size: [{ type: String, required: true }]
  },
  product: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Products"
  }
});

// variantSchema.pre("save", async function(next) {
//   console.log("this", this);
//   try {
//     let foundProduct = await db.Product.findById(this.product);
//     foundProduct.variants.push(this._id);
//     await foundProduct.save();

//     return next();
//   } catch (err) {
//     return next(err);
//   }
// });

module.exports = variantSchema;
