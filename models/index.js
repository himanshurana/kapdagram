const mongoose = require("mongoose");
mongoose.set("debug", true); // It is important if we want to show real mongo query in terminal
mongoose.Promise = Promise; // We will use native es2015 "promises" library so that we don't have to use callback pattern. this line of code is very important because
// we're going to be using es2017 async functions to build this application and async functions return "promises". so
// we really want to make sure that our "mongoose methods" are returning "promises" so everything works.
mongoose.connect(
  "mongodb://rana:rana123@ds221003.mlab.com:21003/kapdagram",
  
  {
    keepAlive: true
  }
);

// "mongodb://127.0.0.1:27017/kapdagram",
  // "mongodb://rana:rana123@ds221003.mlab.com:21003/kapdagram",
// mongoose.connect(
//   "mongodb://localhost:27017/kapdagram",
//   { useNewUrlParser: true }
// );

const adminUserSchema = require("./adminUser");

const departmentSchema = require("./department");
const categorySchema = require("./category");
const genreSchema = require("./genre");
const productSchema = require("./product");
const variantSchema = require("./variant");
const userSchema = require("./user");
const reviewSchema = require("./review");
const addressSchema = require("./address");
const counterSchema = require("./counter")
const orderSchema = require("./Order");

module.exports.AdminUser = mongoose.model("AdminUser", adminUserSchema);

module.exports.Department = mongoose.model("Departments", departmentSchema);
module.exports.Category = mongoose.model("Categories", categorySchema);
module.exports.Genre = mongoose.model("Genres",genreSchema);
module.exports.Product = mongoose.model("Products", productSchema);
module.exports.Variant = mongoose.model("Variants", variantSchema);
module.exports.User = mongoose.model("User", userSchema);
module.exports.Review = mongoose.model("Review", reviewSchema);
module.exports.Address = mongoose.model("Address", addressSchema);
module.exports.counter = mongoose.model("counter", counterSchema);
module.exports.Order = mongoose.model("Order", orderSchema);

// module.exports.Variants = mongoose.model("Variants", variantsSchema);
// const db = {
//   Category: require("./category"),
//   Product: require("./product"),
//   User: require("./user"),
//   Review: require("./review"),
//   Address: require("./address"),
//   books: require("./books"),
//   Order: require("./Order"),
//   StaffPick: require("./staffPick")
// };
// module.exports = db;
