const mongoose = require("mongoose");

var counterSchema = mongoose.Schema({
  totalOrdersQuantity: { type: Number, default: 0 }
});

module.exports = counterSchema;

