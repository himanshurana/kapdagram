const mongoose = require("mongoose");
const db = require("./");

const categorySchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  department: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Departments",
      required: true
    }
  ],
  products: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Products"
    }
  ]
});

categorySchema.pre("remove", async function(next) {
  try {
    // find a user
    console.log(this);
    this.products.map(async product_id => {
      console.log(product_id, await db.Product.find());

      await db.Product.findById(product_id).remove();
    });

    this.department.map(async department_id => {
      console.log(department_id, await db.Department.find());

      let foundDepartment = await db.Department.findById(department_id);
      foundDepartment.categories.remove(this.id);
      await foundDepartment.save();
    });

    // return next
    return next();
  } catch (err) {
    return next(err);
  }
});

module.exports = categorySchema;
