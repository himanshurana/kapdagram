const mongoose = require("mongoose");
const db = require("./");

const productSchema = new mongoose.Schema({
  department: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "departments",
    required: true
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "categories",
    required: true
  },
  genre: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "genres",
    required: true
  },
  title: {
    type: String,
    required: true
  },
  description: String,
  prices: {
    sell_price: { type: Number, required: true },
    mrp: { type: Number },
    discount: { type: Number }
  },
  variants: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Variants",
      required: true
    }
  ],
  specifications: [
    {
      name: { type: String, required: true },
      value: { type: String, required: true }
    }
  ],
  reviews: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Review"
    }
  ]
});

// productSchema.pre("save", async function(next) {
//   console.log(this);
//   try {
//     this.variants.map(async variant => {
//       try {
//         let foundVariant = await db.Variant.findById(variant);

//         foundVariant.product = this._id;
//         await foundVariant.save();
//       } catch (err) {
//         return err;
//       }
//     });
//     // find user who belong to this message
//     // let foundCategory = await db.Category.findById(this.category);
//     // // push this product id into user seleted category
//     // foundCategory.products.push(this.id);
//     // // save upadated category
//     // await foundCategory.save();

//     // let foundGenre = await db.Genre.findById(this.genre);
//     // // push this product id into user selected genre
//     // foundGenre.products.push(this.id);
//     // // save updated genre
//     // await foundGenre.save();

//     let foundDepartment = await db.Department.findById(this.department);
//     // push this product id into user selected department
//     foundDepartment.products.push(this.id);
//     // save upadted department
//     await foundDepartment.save();
//     return next();
//   } catch (err) {
//     return next(err);
//   }
// });

productSchema.pre("remove", async function(next) {
  try {
    this.variants.map(async id=>{
      await db.Variant.findById(id).remove();
    })
    // find category belongs to this product
    let foundCategory = await db.Category.findById(this.category);
    // remove this product id into the foundCategory
    foundCategory.products.remove(this.id);
    await foundCategory.save();

    let foundGenre = await db.Genre.findById(this.genre);
    // push this product id into user selected genre
    foundGenre.products.remove(this.id);
    // save updated genre
    await foundGenre.save();

    let foundDepartment = await db.Department.findById(this.department);
    // push this product id into user selected department
    foundDepartment.products.remove(this.id);
    // save upadted department
    await foundDepartment.save();
    return next();
  } catch (err) {
    return next(err);
  }
});

module.exports = productSchema;
