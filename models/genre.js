const mongoose = require("mongoose");
const db = require("./");

const genreSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  products: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Products"
    }
  ]
});

// genreSchema.pre("remove", async function(next) {
//   try {
//     //find category
//     this.categories.map(async category_id => {
//       await db.Category.findById(category_id).remove();
//     });
//     // return next
//     return next();
//   } catch (err) {
//     return next(err);
//   }
// });

module.exports = genreSchema;
