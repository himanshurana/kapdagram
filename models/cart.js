module.exports = function Cart(oldCart) {
  this.items = oldCart.items || {};
  this.totalQty = oldCart.totalQty || 0;
  this.totalPrice = oldCart.totalPrice || 0;

  this.add = (product, selectedVariant_id, selectedVariant) => {
    let storedItem = this.items[selectedVariant_id];
    let selectedSizesArr = [];

    if (!storedItem) {
      // If the item is not present in the cart, then run this block of code
      selectedSizesArr.push({
        size: selectedVariant.selectedSize,
        qty: 1,
        price: product.prices.sell_price
      });
      selectedVariant.selectedSize = selectedSizesArr;
      storedItem = this.items[selectedVariant_id] = { selectedVariant };
    } else {
      // If the item is present in the cart, then run this block of code
      let findValue = false;
      storedItem.selectedVariant.selectedSize.map((item, index, arr) => {
        if (item.size === selectedVariant.selectedSize) {
          // If the selectedSize is present in the item.size, then run this block of code
          item.qty++;
          item.price = product.prices.sell_price * item.qty;
          findValue = true;
        }
        if (index === arr.length - 1 && !findValue) {
          // If the selectedSize is not present in the item.size, then run this block of code
          let obj = {
            size: selectedVariant.selectedSize,
            qty: 1,
            price: product.prices.sell_price
          };
          storedItem.selectedVariant.selectedSize.push(obj);
        }
      });
    }
    this.totalQty++;
    this.totalPrice += product.prices.sell_price;
    console.log(storedItem.selectedVariant.selectedSize)
    storedItem.product = product;
  };

  this.reduceByOne = async (product, selectedVariant_id, selectedVariant) => {

    this.items[selectedVariant_id].selectedVariant.selectedSize.map(
      async (item, index, arr) => {
        if (item.size === selectedVariant.selectedSize) {
          // If the selectedSize is present in the item.size, then run this block of code
          item.qty--;
          item.price -= product.prices.sell_price;
        }
        if (item.qty <= 0) {
          await arr.splice(index,1);
        }
      });

    this.totalQty--;
    this.totalPrice -= product.prices.sell_price;

    // delete item from cart if any size is not present in selectedSize array
    if (!this.items[selectedVariant_id].selectedVariant.selectedSize.length) {
      await delete this.items[selectedVariant_id];
    }
  };

  this.removeItem = async (variant_id, updateSize) => {
    this.items[variant_id].selectedVariant.selectedSize.map(
      async (item, index, arr) => {
        console.log("yes", item, updateSize, this.totalQty, this.totalPrice );
        if (item.size === updateSize) {
          // If the selectedSize is present in the item.size, then run this block of code
          this.totalQty = this.totalQty - item.qty;
          this.totalPrice = this.totalPrice - item.price;

          let dA = await arr.splice(index, 1);
          console.log("da", dA);
        }
      }
    );
    // delete item from cart if any size is not present in selectedSize array
    if (!this.items[variant_id].selectedVariant.selectedSize.length) {
      await delete this.items[variant_id];
    }
    // delete this.items[id];
  };

  this.generateArray = function() {
    var arr = [];
    for (var id in this.items) {
      arr.push(this.items[id]);
    }
    return arr;
  };
};

// this.add = function(product, variant_id, variant) {
//   var storedItem = this.items[variant_id];
//   if (!storedItem) {
//     storedItem = this.items[variant_id] = {
//       selectedVariant: variant,
//       qty: 0,
//       price: 0
//     };
//   }
//   storedItem.qty++;
//   storedItem.price = product.prices.sell_price * storedItem.qty;
//   this.totalQty++;
//   this.totalPrice += product.prices.sell_price;
//   storedItem.product = product;
// };

// this.reduceByOne = function(product, variant_id) {
//   this.items[variant_id].qty--;
//   this.items[variant_id].price -= product.prices.sell_price;
//   this.totalQty--;
//   this.totalPrice -= product.prices.sell_price;

//   if (this.items[variant_id].qty <= 0) {
//     delete this.items[variant_id];
//   }
// };
