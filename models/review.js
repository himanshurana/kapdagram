const mongoose = require("mongoose");
const db = require("./");

const reviewSchema = new mongoose.Schema(
  {
    text: {
      type: String,
      maxLength: 160
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
    product: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Products"
    }
  },
  {
    timestamps: true  //This will add a created at and an updated at for each individual document that the message schema creates.
  }
);

reviewSchema.pre("remove", async function(next) {
  try {
    // find a user
    let user = await db.User.findById(this.user);
    // remove the id of the message from their messages list
    user.reviews.remove(this.id);
    // save that user
    await user.save();
    // return next
    return next();
  } catch (err) {
    return next(err);
  }
});

module.exports = reviewSchema;
