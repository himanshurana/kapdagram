const mongoose = require("mongoose");
const db = require("./");

const addressSchema = new mongoose.Schema(
  {
    mobile: { type: String, required: true },
    email: { type: String, required: true },
    customer_name: { type: String, required: true },
    customer_address_first_line: { type: String, required: true },
    customer_address_second_line: { type: String, required: true },
    landmark: { type: String, required: true },
    pincode: { type: String, required: true },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    }
  },
  {
    timestamps: true
  }
);

addressSchema.pre("remove", async function(next) {
  // right before "remove" or "delete" the address
  try {
    // find a user
    let user = await db.User.findById(this.user); // "this" refer to that(addressSchema) specific document
    // remove the id of the message from their messages list from user collection
    user.addresss.remove(this.id);
    // save that user
    await user.save();
    // return next
    return next();
  } catch (err) {
    return next(err);
  }
});

//make an Address Collection(Model) in db
module.exports = addressSchema;
