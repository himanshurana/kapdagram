const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema(
  {
    user: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    orderId: { type: Number, required: true },
    // waybill: { type: Number, required: true },
    shipping: {
      mobile: { type: String, required: true },
      email: { type: String, required: true },
      customer_name: { type: String, required: true },
      customer_address_first_line: { type: String, required: true },
      customer_address_second_line: { type: String, required: true },
      landmark: { type: String, required: true },
      pincode: { type: String, required: true }
    },
    tracking: {
      company: { type: String },
      tracking_number: { type: String },
      status: { type: String }
    },
    payment: {
      method: { type: String },
      transaction_id: { type: String }
    },
    status: { type: String, required: true },
    products: { type: Object, required: true }
  },
  {
    timestamps: true //This will add a created at and an updated at for each individual document that the message schema creates.
  }
);

module.exports = orderSchema;
