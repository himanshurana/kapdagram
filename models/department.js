const mongoose = require("mongoose");
const db = require("./");

const departmentSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  categories: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Categories"
    }
  ],
  products: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Products"
    }
  ]
});

departmentSchema.pre("remove", async function(next) {
  try {
    //find category
    this.categories.map(async category_id => {
      await db.Category.findById(category_id).remove();
    });
    // return next
    return next();
  } catch (err) {
    return next(err);
  }
});

module.exports = departmentSchema;
