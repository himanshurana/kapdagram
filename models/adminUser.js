const mongoose = require("mongoose");
const bcrypt = require("bcrypt"); // bcrypt library is going to be used for password hashing.

const adminUserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  }
});

adminUserSchema.pre("save", async function(next) {
  try {
    if (!this.isModified("password")) {
      return next();
    }
    let hashedPassword = await bcrypt.hash(this.password, 10);
    this.password = hashedPassword;
    return next();
  } catch (err) {
    return next(err);
  }
});

adminUserSchema.methods.comparePassword = async function(candidatePassword, next) {
  console.log('cp', candidatePassword)
  try {
    console.log('cp', candidatePassword)
    let isMatch = await bcrypt.compare(candidatePassword, this.password);
    console.log(isMatch)
    return isMatch;
  } catch (err) {
    console.log("err", err)
    return next(err);
  }
};


module.exports = adminUserSchema;
