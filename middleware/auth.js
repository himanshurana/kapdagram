require("dotenv").load(); // for env veriable
var jwt = require("jsonwebtoken");

//make sure the user is logged - Authentication
exports.loginRequired = function(req, res, next) {
  console.log(req.headers.authorization)
  try {
    const token = req.headers.authorization.split(" ")[1]; //Bearer fjlsjadftoken => split(" ")[1] => ["Bearer", "token"]
    jwt.verify(token, process.env.SECRET_KEY, function(err, decoded) {
      console.log(token)
      if (decoded) {
        next();
      } else {
        return next({ status: 401, message: "Please Log In First!!" });
      }
    });
  } catch (e) {
    return next({ status: 401, message: "Please Log In First!" });
  }
};

//make sure we got the currect user - Authorization
exports.ensureCorrectUser = function(req, res, next) {
  try {
    const token = req.headers.authorization.split(" ")[1];
    jwt.verify(token, process.env.SECRET_KEY, function(err, decoded) {
      console.log("decse",decoded)
      if (decoded && decoded.id === req.params.id) {
        return next();
      } else {
        return next({ status: 401, message: "Unauthorized" });
      }
    });
  } catch (e) {
    return next({ status: 401, message: "Unauthorized" });
  }
};