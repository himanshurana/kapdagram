const db = require("../models");
var Cart = require("../models/cart");
// POST CART
exports.createCart = async function(req, res, next) {
  var cart = new Cart(req.session.cart ? req.session.cart : {});
  try {
    let product = req.body;
    let { selectedVariant } = product;
    let selectedVariant_id = selectedVariant.variant._id;
    cart.add(product, selectedVariant_id, selectedVariant);
    req.session.cart = cart;
    const response = {
      products: cart.generateArray(),
      totalQty: cart.totalQty,
      totalPrice: cart.totalPrice
    };
    return res.status(200).json(response);
  } catch (err) {
    res.send(err);
  }
};

// GET CART
exports.getCart = function(req, res) {
  if (!req.session.cart) {
    return res.send({ products: null });
  }
  var cart = new Cart(req.session.cart);
  const response = {
    products: cart.generateArray(),
    totalQty: cart.totalQty,
    totalPrice: cart.totalPrice
  };
  return res.status(200).json(response);
};

exports.reduce = async (req, res) => {
  try {
    // let foundProduct = await db.Product.findById(req.params.product_id);
    // let foundVariant = await db.Variant.findById(req.params.variant_id);
    // var productId = req.params.product_id;
    let product = req.body;
    let { selectedVariant } = product;
    let selectedVariant_id = selectedVariant.variant._id;
    let cart = new Cart(req.session.cart ? req.session.cart : {});

    await cart.reduceByOne(product, selectedVariant_id, selectedVariant);
    req.session.cart = cart;
    if (cart.totalQty) {
      const response = await {
        products: cart.generateArray(),
        totalQty: cart.totalQty,
        totalPrice: cart.totalPrice
      };
      return res.status(200).json(response);
    } else {
      const response = {};
      return res.status(200).json(response);
    }

    console.log("res", response);
  } catch (err) {
    return res.send(err);
  }
};

exports.deleteCart = function(req, res) {
  let data = req.body;

  let cart = new Cart(req.session.cart ? req.session.cart : {});
  let { variant_id, updateSize } = req.body;
  cart.removeItem(variant_id, updateSize);
  req.session.cart = cart;
  const response = {
    products: cart.generateArray(),
    totalQty: cart.totalQty,
    totalPrice: cart.totalPrice
  };
  return res.status(200).json(response);
};
