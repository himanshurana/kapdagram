const db = require("../models");
var {
  paytm_config
} = require("../Paytm_App_Checksum_Kit_NodeJs-master/paytm/paytm_config");
var paytm_checksum = require("../Paytm_App_Checksum_Kit_NodeJs-master/paytm/checksum");

exports.generateChecksum = async function(request, response, next) {
  try {
    var paramarray = {};
    paramarray["MID"] = "xxxxxxxxxxxxxx"; //Provided by Paytm
    paramarray["ORDER_ID"] = "ORDER00001"; //unique OrderId for every request
    paramarray["CUST_ID"] = "CUST0001"; // unique customer identifier
    paramarray["INDUSTRY_TYPE_ID"] = "xxxxxxxxx"; //Provided by Paytm
    paramarray["CHANNEL_ID"] = "WAP"; //Provided by Paytm
    paramarray["TXN_AMOUNT"] = "1.00"; // transaction amount
    paramarray["WEBSITE"] = "xxxxxxxxxxxx"; //Provided by Paytm
    paramarray["CALLBACK_URL"] =
      "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp"; //Provided by Paytm
    paramarray["EMAIL"] = "abc@gmail.com"; // customer email id
    paramarray["MOBILE_NO"] = "9999999999"; // customer 10 digit mobile no.
    paytm_checksum.genchecksum(paramarray, paytm_config.MERCHANT_KEY, function(
      err,
      res
    ) {
        response.writeHead(200, {
        "Content-type": "text/json",
        "Cache-Control": "no-cache"
      });
      response.write(JSON.stringify(res));
      response.end();
    });
  } catch (err) {
    return next(err);
  }
};

// exports.getGenres = async function(req, res, next) {
//   try {
//     let genres = await db.Genre.find();
//     return res.status(200).json(genres);
//   } catch (err) {
//     return next(err);
//   }
// };

// exports.deleteGenre = async function(req, res, next) {
//   try {
//     const foundGenre = await db.Genre.findById(req.params.id);
//     await foundGenre.remove();
//     return res.status(200).json(foundGenre);
//   } catch (err) {
//     return next(err);
//   }
// };
// exports.updateGenre = async function(req, res) {
//   console.log(req.params.id, req.body);
//   var query = req.params.id;
//   // if the field doesn't exist $set will set a new field
//   var update = {
//     $set: {
//       title: req.body.title,
//       description: req.body.description
//     }
//   };
//   // When true returns the updated document
//   var options = { new: true };

//   db.Genre.findOneAndUpdate(query, update, options, function(err, Genre) {
//     if (err) {
//       throw err;
//     }
//     res.json(Genre);
//   });
// };
