const db = require("../models");
const jwt = require("jsonwebtoken");

exports.signin = async function(req, res, next) {
  try {
    // finding a user
    let user = await db.User.findOne({
      email: req.body.email
    })
      .populate("reviews", { text: true })
      .populate("addresses", { address: true });
    let { id, email, reviews, addresses } = user;
    let isMatch = await user.comparePassword(req.body.password);
    if (isMatch) {
      let token = jwt.sign(
        {
          id,
          email,
          addresses
        },
        process.env.SECRET_KEY
      );
      return res.status(200).json({
        id,
        email,
        addresses,
        token
      });
    } else {
      return next({
        status: 400,
        message: "Invalid Email/Password."
      });
    }
  } catch (e) {
    return next({ status: 400, message: "Invalid Email/Password. hjkh" });
  }
};

exports.signup = async function(req, res, next) {
  try {
    console.log(req.body);
    // create a user
    let user = await db.User.create(req.body);
    let { id, email } = user;
    // create a token (signing a token)
    let token = jwt.sign(
      {
        id,
        email
      },
      process.env.SECRET_KEY
    );
    return res.status(200).json({
      id,
      email,
      token
    });
  } catch (err) {
    console.log(err);
    // if a validation fails!
    if (err.code === 11000) {
      err.message = "Sorry, that email is taken";
    }
    return next({
      status: 400,
      message: err.message
    });
  }
};
