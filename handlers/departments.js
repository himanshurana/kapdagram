const db = require("../models");

// exports.createDepartment = async function(req, res, next) {
//   try {
//     let department = await db.Department.create({
//       title: req.body.title,
//       description: req.body.description,
//       categories: req.body.category
//     });
//     return res.status(200).json(department);
//   } catch (err) {
//     return next(err);
//   }
// };

exports.getDepartments = async function(req, res, next) {
  try {
    let departments = await db.Department.find().populate("categories");
    return res.status(200).json(departments);
  } catch (err) {
    return next(err);
  }
};

// exports.deleteDepartment = async function(req, res, next) {
//   try {
//     const foundDepartment = await db.Department.findById(req.params.id);
//     await foundDepartment.remove();
//     return res.status(200).json(foundDepartment);
//   } catch (err) {
//     return next(err);
//   }
// };
// exports.updateDepartment = async function(req, res) {
//   var query = req.params.id;

//   // When true returns the updated document
//   var options = { new: true };

//   let updatedDepartment = await db.Department.findOneAndUpdate(
//     query,
//     { $set: req.body },
//     options
//   );
//   return res.json(updatedDepartment);
// };
