const db = require("../models");
var Cart = require("../models/cart");
const config = require("../config/keys");
const axios = require("axios");
const { createPackage, trackPackage } = require("./delhivery");

exports.getOrders = async function(req, res) {
  try {
    // let orders = await db.Order.find();
    let foundUser = await db.User.findById(req.params.id).populate("orders");
    console.log(foundUser.orders);
    res.send(foundUser.orders);
  } catch (err) {
    res.send(err);
  }
};

exports.trackOrder = async (req, res, next) => {
  let foundOrder = await db.Order.findById(req.params.order_id);
  // await trackPackage(req, res, next, foundOrder);
  res.send(foundOrder);
};

exports.cancelOrder = async (req, res, next) => {
  try {
    console.log("update", req.body, req.params);
    var order = req.body;
    // When true returns the updated document
    var options = { new: true };

    db.Order.findOneAndUpdate(
      { _id: req.params.order_id },
      { $set: req.body },
      options,
      function(err, orders) {
        if (err) {
          throw err;
        }
        res.json(orders);
      }
    );
  } catch (err) {
    res.send(err);
  }
};

exports.createOrder = async (req, res, next) => {
  try {
    // console.log(req.body.payment.method.toLowerCase());

    switch (req.body.payment.method.toLowerCase()) {
      case "cod":
        if (!req.session.cart) {
          return console.log("rr");
        }
        var cart = new Cart(req.session.cart);

        const productsObject = {
          items: cart.generateArray(),
          totalQty: cart.totalQty,
          totalPrice: cart.totalPrice
        };
        let user = req.params.id;
        let products = productsObject;
        let shipping = req.body.shippingAddress;
        let payment = req.body.payment;
        let order_date = new Date().toUTCString();
        // send order on delhivery create package API
        // let createPackageResponse = await createPackage(
        //   req,
        //   res,
        //   next,
        //   order_date,
        //   shipping,
        //   products,
        //   payment
        // );

        // check success status into delhivery create package API response
        // if (createPackageResponse.success) {
        // if order posted successfuly on delivery create package API then run this block of code
        // find counter module
        let foundCounter = await db.counter.find();
        // find counter document then increae it's totalOrderQuantity value by 1 then update the document
        let updatedCounter = await db.counter.findByIdAndUpdate(
          { _id: foundCounter[0]._id },
          { $inc: { totalOrdersQuantity: 1 } }
        );

        // after updating the counder model document make a order document with orderId and all properties.
        var createdOrder = await db.Order.create({
          user,
          products,
          shipping,
          payment,
          status: "Processing Stock",
          orderId: updatedCounter.totalOrdersQuantity
        });

        // find user who belong to this message
        let foundUser = await db.User.findById(user);
        //push this message into found user
        foundUser.orders.push(createdOrder._id);
        // then save the updated user
        await foundUser.save();
        console.log("final", foundUser);
        // after create a order documnet successfully now clear the cart
        req.session.cart = null;
        // send the created Order to client
        res.send(createdOrder);
      // }
    }
  } catch (err) {
    res.send(err);
  }
};
