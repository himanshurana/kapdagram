const db = require("../models");
const axios = require("axios");
const { delhiveryKey } = require("../config/keys");
const data = require("../postPackageData.json");
const config = require("../config/keys");

exports.checkPincode = async (req, res, next) => {
  try {
    console.log(req.params, delhiveryKey);
    let pincode = req.params.pincode;
    let URL = `http://test.delhivery.com/c/api/pin-codes/json/?token=${delhiveryKey}&filter_codes=${pincode}`;
    let response = await axios.get(URL);
    console.log("response",pincode, response);
    res.send(response.data);
  } catch (err) {
    next(err);
  }
};

exports.trackPackage = async (req, res, next, foundOrder) => {
  try {
    // let response = await axios.get(
    //   `http://test.delhivery.com/api/packages/json/?token=${
    //     config.delhiveryKey
    //   }&waybill=${foundOrder.waybill}%201&verbose=0`
    // );
    let response = {
      ShipmentData: [
        {
          Shipment: {
            Origin: "origin center",
            Status: {
              Status: "shipment status",
              StatusLocation: "shipment location",
              StatusDateTime: "status datetime",
              RecievedBy: "received by",
              StatusCode: "status code",
              StatusType: "status type",
              Instructions: "instructions"
            },
            PickUpDate: "date of pickup",
            InvoiceAmount: "invoice amount",
            OrderType: "type of order",
            Destination: "shipment destination",
            Consignee: {
              City: "consignee city",
              State: "consignee state",
              Telephone2: "consignee telephone2",
              Name: "consignee name",
              Address1: "consignee address",
              Telephone1: "consignee telephone1",
              Country: "consignee country",
              Address2: "consignee address",
              Address3: "consignee address",
              PinCode: "consignee pincode"
            },
            ReferenceNo: "order id",
            ReturnedDate: "shipment return date",
            DestRecieveDate: "destination received date",
            ReverseInTransit: "",
            OutDestinationDate: "",
            CODAmount: "cod amount",
            ChargedWeight: "shipment charged weight",
            OriginRecieveDate: "origin received date",
            Scans: [
              {
                ScanDetail: {
                  ScanDateTime: "shipment status",
                  ScanType: "shipment location",
                  Scan: "status datetime",
                  StatusDateTime: "received by",
                  ScannedLocation: "instructions",
                  StatusCode: "status code",
                  Instructions: "status type"
                }
              }
            ],
            SenderName: "seller name",
            AWB: "waybill no.",
            DispatchCount: "no. of times dispatched",
            FirstAttemptDate: "first attempt date"
          }
        }
      ]
    };
    let orderObj = {
      order: foundOrder,
      orderTrackingDetails: response
    };
    res.send(orderObj);
  } catch (err) {
    next(err);
  }
};

exports.createPackage = async (
  req,
  res,
  next,
  order_date,
  shipping,
  products,
  payment_mode
) => {
  try {
    let createOrder = await axios.post(
      "http://test.delhivery.com/api/cmu/create.json",
      {
        "format=json&data": {
          "shipments": [
            {
              "tax_value": 90,
              "city": "delhi",
              "weight": 50,
              "return_state": "delhi",
              "add": "hosue no. 315, khera kalan",
              "address_type": "home",
              "phone": 8802869692,
              "payment_mode": "cod",
              "name": "t-shirt",
              "seller_name": "KAPDAGRAM",
              "return_city": "delhi",
              "return_phone": 8802869692,
              "category_of_goods": "t-shirt",
              "cod_amount": 500,
              "return_country": "delhi",
              "shipment_width": "26",
              "pin": 110082,
              "state": "Delhi",
              "order_date": "2019-01-20 12:00:00",
              "return_add": "mekash house address",
              "total_amount": 500,
              "seller_add": "mekash hosue address",
              "country": "India",
              "return_pin": 110086,
              "client": "KAPDAGRAM SURFACE",
              "return_name": "KAPDAGRAM SURFACE",
              "order": "2000",
              "quantity": "1"
            }
          ],
          "pickup_location": {
            "city": "delhi",
            "name": "KAPDAGRAM SURFACE",
            "pin": "110086",
            "country": "India",
            "phone": "8800428765",
            "add":
              "left portion, first floor,plot no 75-16,pocket-1,rohini,sector 20, New Delhi, DELHI, 110086, India."
          }
        }
      },
      {
        headers: {
          Accept: "application/json",
          Authorization: "Token 0599c62f7030d1381c46ed4e670f03051d0ba084" //the token is a variable which holds the token
        }
      }
    );
    console.log("createOrder", createOrder);
    return { success: true };
  } catch (err) {
    console.log(err);
  }
};

exports.createPackingSlip = async (req, res) => {
  try {
    let createOrder = await axios.post(
      `http://test.delhivery.com/api/p/packing_slip/?wbns=${123}`,
      {
        headers: {
          Accept: "application/json",
          Authorization: "Token 0599c62f7030d1381c46ed4e670f03051d0ba084" //the token is a variable which holds the token
        }
      }
    );
  } catch (err) {
    res.send(err);
  }
};

exports.editOrCancelPackage = async (req, res, order) => {
  let editedPackage = await axios.post("http://test.delhivery.com/api/p/edit", {
    order
  });
};

// let foundCounter = await db.counter.find();
    // let order_id = foundCounter[0].totalOrdersQuantity;
    // let data = {
    //   shipments: [
    //     {
    //       tax_value: 90,
    //       city: shipping.city,
    //       weight: 50,
    //       return_state: "delhi",
    //       phone: shipping.phone,
    //       name: shipping.name,
    //       add: shipping.address,
    //       pin: shipping.pincode,
    //       state: shipping.state,
    //       address_type: shipping.addressType,
    //       payment_mode: payment_mode,
    //       category_of_goods: order,
    //       cod_amount: products.totalPrice,
    //       order_date: order_date,
    //       shipment_width: "26",
    //       quantity: products.quantity,
    //       total_amount: products.totalPrice,
    //       order: order_id+1,
    //       seller_name: "KAPDAGRAM",
    //       seller_add: "mekash hosue address",
    //       return_phone: 8802869692,
    //       return_name: "KAPDAGRAM SURFACE",
    //       return_city: "delhi",
    //       return_country: "India",
    //       return_add: "mekash house address",
    //       country: "India",
    //       return_pin: 110086,
    //       client: "KAPDAGRAM SURFACE",
    //     }
    //   ],
    //   pickup_location: {
    //     city: "delhi",
    //     name: "KAPDAGRAM SURFACE",
    //     pin: "110086",
    //     country: "India",
    //     phone: "8800428765",
    //     add:
    //       "left portion, first floor,plot no 75-16,pocket-1,rohini,sector 20, New Delhi, DELHI, 110086, India."
    //   }
    // };