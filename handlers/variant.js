const db = require("../models");
var fs = require("fs");

const callbacks = {
  updateVariant: async (req, res) => {
    var variant_id = req.params.variant_id;
    // console.log("var", variant_id, req.body, req.files);

    let variantData = {};

    if (!!req.files.length) {
      let foundVariant = await db.Variant.findById(variant_id);
      // console.log(foundVariant)
      await foundVariant.assests.imgs.map(imgLink => {
        var filename = imgLink.substring(imgLink.lastIndexOf("/") + 1);

        fs.unlink(filename, function(err) {
          if (err) {
            console.log(err);
          }
          // if no error, file has been deleted successfully
          console.log("File deleted!");
        });
      });
      let imageFilesPathArr = [];
      req.files.map(item => {
        imageFilesPathArr.push(
          req.protocol + "://" + req.get("host") + "/" + item.path
        );
      });

      variantData.assests = {
        imgs: imageFilesPathArr
      };
    }

    if (!!req.body.sizes) {
      let sizeArr = req.body.sizes.split(",");
      variantData.attributes = {
        size: sizeArr,
        color: req.body.color
      };
    }

    // When true returns the updated document
    var options = { new: true };

    let updatedVariant = await db.Variant.findOneAndUpdate(
      { _id: variant_id },
      { $set: variantData },
      options
    );
    res.json(updatedVariant);
  },
  createVariant: async (req, res) => {
    console.log("cV", req.body, req.files);
    let imageFilesPathArr = [];
    req.files.map(item => {
      imageFilesPathArr.push(
        req.protocol + "://" + req.get("host") + "/" + item.path
      );
    });
    let sizeArr = req.body.sizes.split(",");
    let createdVariant = await db.Variant.create({
      assests: {
        imgs: imageFilesPathArr
      },
      attributes: {
        color: req.body.color,
        size: sizeArr
      },
      product: req.body.product_id
    });

    res.send(createdVariant);
  },
  deleteVariant: async (req, res) => {
    let foundVariant = await db.Variant.findById(req.params.variant_id);

    await foundVariant.assests.imgs.map(imgLink => {
      var filename = imgLink.substring(imgLink.lastIndexOf("/") + 1);

      fs.unlink(filename, function(err) {
        if (err) {
          console.log(err);
        }
        // if no error, file has been deleted successfully
        console.log("File deleted!");
      });
    });
    await foundVariant.remove();
    res.send("delete")
  }
};

module.exports = callbacks;
