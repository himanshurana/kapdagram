const db = require("../models");
var fs = require("fs");

const callbacks = {
  // postProducts: async function(req, res, next) {
  //   try {
  //     console.log("Pd", req.body, req.files)
  //     let variantIdsArr = [];
  //     let product = JSON.parse(req.body.product);

  //     product.variants.map(async (variant, i, arr) => {
  //       let imgsArr = [];

  //       req.files.map(file => {
  //         if (variant.color === file.originalname) {
  //           // Push image into imgArr if file.originalname have same varinat color value
  //           imgsArr.push(
  //             req.protocol + "://" + req.get("host") + "/" + file.path
  //           );
  //         }
  //       });

  //       let foundVariant = await new db.Variant({
  //         // sku: variant.sku,
  //         assests: {
  //           imgs: imgsArr
  //         },
  //         attributes: {
  //           color: variant.color,
  //           size: variant.size
  //         }
  //       }).save();
  //       await variantIdsArr.push(foundVariant._id);
  //     });
  //     // //wait for 10 seconds so that all mongoose queries(Variant Create and Save) have been resolved.
  //     setTimeout(async () => {
  //       try {
  //         // make product after creating the all variant
  //         //this block of code only run when map function reached on the last variant of the variants array
  //         let createProduct = await new db.Product({
  //           department: product.department,
  //           category: product.category,
  //           genre: product.genre,
  //           title: product.title,
  //           description: product.description,
  //           variants: variantIdsArr,
  //           prices: {
  //             sell_price: product.sell_price
  //             // mrp: product.mrp,
  //             // discount: calculateDiscountPercentage(
  //             //   product.sell_price,
  //             //   product.mrp
  //             // )
  //           },
  //           specifications: product.specifications
  //         }).save();

  //         await createProduct.variants.map(async variant => {
  //           try {
  //             let foundVariant = await db.Variant.findById(variant);

  //             foundVariant.product = product._id;
  //             await foundVariant.save();
  //           } catch (err) {
  //             return err;
  //           }
  //         });
  //         // find user who belong to this message
  //         let foundCategory = await db.Category.findById(product.category);
  //         //push this message into found user
  //         foundCategory.products.push(createProduct.id);
  //         // then save the updated user
  //         await foundCategory.save();

  //         re = await db.Genre.findById(product.genre);
  //         // push this product id into user selected genre
  //         foundGenre.products.push(product.id);
  //         // save updated genre
  //         await foundGenre.save();

  //         let foundDepartment = await db.Department.findById(
  //           product.department
  //         );
  //         // push this product id into user selected department
  //         foundDepartment.products.push(product.id);
  //         // save upadted department
  //         await foundDepartment.save();
  //         return res.status(200).json(createProduct);
  //       } catch (err) {
  //         return res.send(err);
  //       }
  //     }, 10000);
  //   } catch (err) {
  //     return next(err);
  //   }
  // },
  getProducts: async function(req, res, next) {
    try {
      // fs.unlink('./uploads/40592159_2156635587744845_5265889535669043200_n.jpg', function (err) {
      //   if (err) throw err;
      //   // if no error, file has been deleted successfully
      //   console.log('File deleted!');
      // });
      let products = await db.Product.find()
        .populate("categories")
        .populate("variants")
        .populate("reviews");
      return res.status(200).json(products);
    } catch (err) {
      return next(err);
    }
  },
  getProduct: async function(req, res, next) {
    try {
      let foundProduct = await db.Product.findById(req.params.id)
        .populate("products")
        .populate("variants");
      return res.status(200).json(foundProduct);
    } catch (err) {
      return next(err);
    }
  },
//   deleteProducts: async function(req, res, next) {
//     try {
//       let foundProduct = await db.Product.findById(req.params.id);
//       console.log("req.params.id", foundProduct, req.params.id);
//       await foundProduct.remove();

//       return res.status(200).json(foundProduct);
//     } catch (err) {
//       return next(err);
//     }
//   },
//   updateProducts: async function(req, res) {
//     var product = req.body;
//     // When true returns the updated document
//     var options = { new: true };

//     db.Product.findOneAndUpdate(
//       { _id: req.params.id },
//       { $set: product },
//       options,
//       function(err, products) {
//         if (err) {
//           throw err;
//         }
//         res.json(products);
//       }
//     );
//   }
};

module.exports = callbacks;
