const db = require("../models");

exports.createAddress = async function(req, res, next) {
  try {
    console.log(req.body);
    const {
      mobile,
      email,
      customer_name,
      customer_address_first_line,
      customer_address_second_line,
      landmark,
      pincode
    } = req.body;
    let createdAddress = await db.Address.create({
      mobile,
      email,
      customer_name,
      customer_address_first_line,
      customer_address_second_line,
      landmark,
      pincode,
      user: req.params.id
    });
    let foundUser = await db.User.findById(req.params.id).populate("addresses");
    await foundUser.addresses.push(createdAddress.id);
    await foundUser.save();
    const { addresses } = foundUser.populate("addresses");
    return res.status(200).json(addresses);
  } catch (err) {
    return next(err);
  }
};

exports.getAddress = async function(req, res, next) {
  try {
    let foundUser = await db.User.findById(req.params.id).populate("addresses");
    const { addresses } = foundUser;
    return res.status(200).json(addresses);
  } catch (err) {
    return next(err);
  }
};

// DELETE /api/users/:id/messages/:message_id
exports.deleteAddress = async function(req, res, next) {
  try {
    console.log(req.params.id);
    let foundAddress = await db.Address.findById(req.params.id).remove();

    return res.status(200).json(foundAddress);
  } catch (err) {
    return next(err);
  }
};
