const db = require("../../models");

exports.createGenre = async function(req, res, next) {
  console.log("req.body")
  try {
    let Genre = await db.Genre.create(req.body);
    return res.status(200).json(Genre);
  } catch (err) {
    return next(err);
  }
};

exports.deleteGenre = async function(req, res, next) {
  try {
    const foundGenre = await db.Genre.findById(req.params.id);
    await foundGenre.remove();
    return res.status(200).json(foundGenre);
  } catch (err) {
    return next(err);
  }
};
exports.updateGenre = async function(req, res) {
  console.log(req.params.id, req.body);
  var query = req.params.id;
  // if the field doesn't exist $set will set a new field
  var update = {
    $set: {
      title: req.body.title,
      description: req.body.description
    }
  };
  // When true returns the updated document
  var options = { new: true };

  db.Genre.findOneAndUpdate(query, update, options, function(err, Genre) {
    if (err) {
      throw err;
    }
    res.json(Genre);
  });
};
