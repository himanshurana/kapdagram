const db = require("../../models");
const jwt = require("jsonwebtoken");

exports.signin = async function(req, res, next) {
  try {
    // finding a user
    let adminUser = await db.AdminUser.findOne({
      email: req.body.email
    })
    console.log("au", adminUser)
    let { id, email } = adminUser;
    let isMatch = await adminUser.comparePassword(req.body.password);
    console.log("im", isMatch)
    if (isMatch) {
      let token = jwt.sign(
        {
          id,
          email
        },
        process.env.SECRET_KEY
      );
      console.log("tk", token)
      return res.status(200).json({
        id,
        email,
        token
      });
    } else {
      return next({
        status: 400,
        message: "Invalid Email/Password."
      });
    }
  } catch (e) {
    return next({ status: 400, message: "Invalid Email/Password." });
  }
};

exports.signup = async function(req, res, next) {
  try {
    // create a user
    let adminUser = await db.AdminUser.create({email: "rana@kapdagram.com", password: "123"});
    let { id,email } = adminUser;
    // create a token (signing a token)
    let token = jwt.sign(
      {
        id,
        email
      },
      process.env.SECRET_KEY
    );
    return res.status(200).json({
      id,
      email,
      token
    });
  } catch (err) {
    // if a validation fails!
    if (err.code === 11000) {
      err.message = "Sorry, that email is taken";
    }
    return next({
      status: 400,
      message: err.message
    });
  }
};

exports.adminSignup = async function(req, res, next) {
  try {
    // create a user
    let adminUser = await db.AdminUser.create({
      email: "abc@abc.com",
      password: `8802869692`
    });
    let { id, email } = adminUser;
    // create a token (signing a token)
    let token = jwt.sign(
      {
        id,
        email
      },
      process.env.SECRET_KEY
    );
    return res.status(200).json({
      id,
      email,
      token
    });
  } catch (err) {
    // if a validation fails!
    if (err.code === 11000) {
      err.message = "Sorry, that username and/or email is taken";
    }
    return next({
      status: 400,
      message: err.message
    });
  }
};
