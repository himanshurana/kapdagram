const db = require("../../models");

exports.createCategory = async function(req, res, next) {
  try {
    let createdCategory = await db.Category.create(req.body);

     // find user who belong to this message
     let foundDepartment = await db.Department.findById(req.body.department);
     // push this product id into user seleted category
     foundDepartment.categories.push(createdCategory.id);
     // save upadated category
     await foundDepartment.save();
    return res.status(200).json(createdCategory);
  } catch (err) {
    return next(err);
  }
};

exports.deleteCategory = async function(req, res, next) {
  try {
    const foundCategory = await db.Category.findById(req.params.id);
    await foundCategory.remove();
    return res.status(200).json(foundCategory);
  } catch (err) {
    return next(err);
  }
};
exports.updateCategory = async function(req, res) {
  var query = req.params.id;

  // When true returns the updated document
  var options = { new: true };

  db.Category.findOneAndUpdate(query, { $set: req.body}, options, function(err, category) {
    if (err) {
      throw err;
    }
    res.json(category);
  });
};
