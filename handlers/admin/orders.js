const db = require("../../models");
const axios = require("axios");

exports.getOrders = async function(req, res) {
  console.log("orders");
  const orders = await db.Order.find();
  res.send(orders);
};

exports.updateOrderStatus = async (req, res) => {
  // When true returns the updated document
  var options = { new: true };
  console.log("lajfdla", req.body)
  db.Order.findOneAndUpdate(
    { _id: req.params.order_id },
    { $set: req.body },
    options,
    function(err, orders) {
      if (err) {
        throw err;
      }
      console.log(orders)
      res.json(orders);
    }
  );
};

exports.cancelOrder = async (req, res, next) => {
  try {
    const removeOrder = await db.Order.findByIdAndRemove(req.params.order_id);
    res.send(removeOrder);
  } catch (err) {}
};