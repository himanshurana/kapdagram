import { apiCall } from "../../services/api";
import {
  GET_DEPARTMENTS,
} from "../actionTypes";

export function getDepartments() {
  return dispatch => {
    return apiCall("get", "/api/departments")
      .then(res => {
        dispatch({
          type: GET_DEPARTMENTS,
          payload: res
        });
      })
      .catch(err => {
        console.log(err)
      });
  };
}
