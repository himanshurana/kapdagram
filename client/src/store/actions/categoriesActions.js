import { addError, removeError } from "./errors";
import { apiCall } from "../../services/api";
import { GET_CATEGORIES } from "../actionTypes";

export function getCategories() {
  return dispatch => {
    return apiCall("get", "/api/categories")
      .then(res => {
        dispatch({
          type: GET_CATEGORIES,
          payload: res
        });
      })
  };
}
