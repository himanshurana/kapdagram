import { POST_ADDRESS, GET_ADDRESS, REMOVE_ADDRESS } from "../actionTypes";
import { apiCall } from "../../services/api";

export function postAddress(user_id, address, history) {
  console.log("address", address);
  return dispatch => {
    apiCall(
      "post",
      `/api/users/${user_id}/address`,
      address
    )
      .then(res => {
        history.push("/checkout_address")
        // dispatch({
        //   type: POST_ADDRESS,
        //   payload: res
        // });
      })
      .catch(err => {
        // dispatch({
        //   type: "POST_ADDRESS_REJECTED",
        //   msg: err
        // });
      });
  };
}

export function getAddress(user_id) {
  return dispatch => {
    apiCall("get", `/api/users/${user_id}/address`)
      .then(res => {
        dispatch({
          type: GET_ADDRESS,
          payload: res
        });
      })
      .catch(err => {
        dispatch({
          type: "GET_ADDRESS_REJECTED",
          msg: err
        });
      });
  };
}

export function removeAddress(user_id, address_id) {
  return dispatch => {
    apiCall(
      "delete",
      `/api/users/${user_id}/address/${address_id}`
    )
      .then(res => {
        dispatch({
          type: REMOVE_ADDRESS,
          payload: res
        });
      })
      .catch(err => {
        dispatch({
          type: "REMOVE_ADDRESS_REJECTED",
          msg: err
        });
      });
  };
}
