import axios from "axios";
import { addError } from "./errors";
import { apiCall } from "../../services/api";
import {
  LOAD_PRODUCTS,
  GET_PRODUCT
} from "../actionTypes";

export const verifyPincode = pincode => {
  return dispatch => {
    return apiCall("get", `/check-pincode/${pincode}`)
      .then(res => {
        if (res.delivery_codes.length) {
          localStorage.setItem("verifyedPincode", "true");
        } else {
          localStorage.setItem("verifyedPincode", "false");
        }
      })
      .catch(err => {
        dispatch(addError(err.message));
      });
  };
};

export const getProduct = product_id => {
  return dispatch => {
    return apiCall("get", `/api/products/${product_id}`)
      .then(res => {
        dispatch({
          type: GET_PRODUCT,
          payload: res
        });
      })
      .catch(err => {
        console.log(err);
      });
  };
};

export const getProducts = () => {
  return dispatch => {
    return apiCall("get", "/api/products")
      .then(res => {
        dispatch({
          type: LOAD_PRODUCTS,
          payload: res
        });
      })
      .catch(err => {
        console.log(err);
      });
  };
};
