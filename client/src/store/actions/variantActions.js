import axios from "axios";

export const createVariant = (data) => {
    // axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';
    var config = {
      headers: { "Content-Type": "multipart/form-data" }
    };
    return dispatch => {
      return axios
        .post(`http://localhost:8589/api/variant`, data, config)
        .then(res => {
          console.log("createdVariant", res)
          // dispatch({
          //   type: _PRODUCT,
          //   payload: res
          // });
        })
        .catch(err => {
          console.log(err.message);
        });
    };
  };

export const updateVariant = (variant_id, updatedData) => {
    // axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';
    var config = {
      headers: { "Content-Type": "multipart/form-data" }
    };
    return dispatch => {
      return axios
        .put(`http://localhost:8589/api/variant/${variant_id}`, updatedData, config)
        .then(res => {
          console.log("updatedVariant", res)
          // dispatch({
          //   type: _PRODUCT,
          //   payload: res
          // });
        })
        .catch(err => {
          console.log(err.message);
        });
    };
  };

  export const deleteVariant = (variant_id) => {
    return dispatch => {
      return axios
        .delete(`http://localhost:8589/api/variant/${variant_id}`)
        .then(res => {
          console.log("deleteVariant", res)
          // dispatch({
          //   type: _PRODUCT,
          //   payload: res
          // });
        })
        .catch(err => {
          console.log(err.message);
        });
    };
  };