import { apiCall } from "../../services/api";
import { GET_GENRES } from "../actionTypes";

export function getGenres() {
  return dispatch => {
    return apiCall("get", "/api/genres")
      .then(res => {
        dispatch({
          type: GET_GENRES,
          payload: res
        });
      })
      .catch(err =>
        dispatch({
          type: GET_GENRES,
          payload: err
        })
      );
  };
}
