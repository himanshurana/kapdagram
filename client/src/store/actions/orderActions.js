import { apiCall } from "../../services/api";
import {
  POST_ORDER,
  GET_ORDERS,
  CANCEL_ORDER,
  TRACK_ORDER
} from "../actionTypes";

export const postOrder = (user_id, checkoutObject, history) => {
  console.log(checkoutObject, user_id);
  return dispatch => {
    apiCall("post", `/api/users/${user_id}/orders`, checkoutObject)
      .then(res => {
        history.push("/orders");
        dispatch({
          type: POST_ORDER,
          payload: res
        });
      })
      .catch(err => {
        console.log(err);
      });
  };
};
export const getOrders = user_id => {
  return dispatch => {
    apiCall("get", `/api/users/${user_id}/orders`)
      .then(res => {
        dispatch({
          type: GET_ORDERS,
          payload: res
        });
      })
      .catch(err => {
        console.log(err);
      });
  };
};
export const trackOrder = (user_id, order_id) => {
  console.log("req", user_id, order_id);
  return dispatch => {
    apiCall("get", `/api/users/${user_id}/orders/${order_id}`)
      .then(res => {
        dispatch({
          type: TRACK_ORDER,
          payload: res
        });
      })
      .catch(err => {
        return console.log(err);
      });
  };
};

export const cancelOrder = (user_id, order_id) => {
  return dispatch => {
    apiCall("put", `/api/users/${user_id}/orders/${order_id}`, {status: "cancel"})
      .then(res => {
        dispatch({
          type: CANCEL_ORDER,
          payload: res
        });
      })
      .catch(err => {
        console.log(err);
      });
  };
};