import { GET_CART, POST_CART } from "../actionTypes";
import { apiCall } from "../../services/api";
import { addError, removeError } from "./errors";

export function getCart() {
  return function(dispatch) {
    apiCall("get", "/api/cart")
      .then(res => {
        dispatch({
          type: GET_CART,
          payload: res
        });
      })
      .catch(err => {
        console.log(err);
      });
  };
}
// ADD TO CART
export function addToCart(product, history) {
  console.log("product", product);
  return function(dispatch) {
    apiCall("post", `/api/add-to-cart/`, product)
      .then(res => {
        dispatch({
          type: POST_CART,
          payload: res
        });
        history.push("/cart");
      })
      .catch(err => {
        console.log(err);
      });
  };
}

// REDUCE ITEM QUANTITY
export function reduce(product) {
  return function(dispatch) {
    apiCall("post", `/api/cart/reduce_item`, product)
      .then(res => {
        dispatch({
          type: GET_CART,
          payload: res
        });
      })
      .catch(err => {
        console.log(err);
      });
  };
}

// DELETE CART
export function onRemove(data) {
  return function(dispatch) {
    apiCall("post", `/api/cart/remove_item`, data)
      .then(res => {
        dispatch({
          type: GET_CART,
          payload: res
        });
      })
      .catch(err => {
        console.log(err);
      });
  };
}
