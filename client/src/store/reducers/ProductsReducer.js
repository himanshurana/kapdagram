import {
  LOAD_PRODUCTS,
  POST_PRODUCT,
  DELETE_PRODUCT,
  GET_PRODUCT
} from "../actionTypes";

export const productReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_PRODUCT:
      return action.payload;
    default:
      return state;
  }
};

//productS REDUCERS
export const productsReducer = (state = { products: [] }, action) => {
  switch (action.type) {
    case LOAD_PRODUCTS:
      return { ...state, products: action.payload };
  }
  return state;
};
