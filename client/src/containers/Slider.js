import React, { Component } from "react";
import Slider from "react-slick";

import "../CSS/slider.css";

export class SimpleSlider extends Component {
  render() {
    let settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true
    };
    return (
      <div>
        {/* <h2> Single Item</h2> */}
        <Slider {...settings}>
          <div>
            {/* <h3>1</h3> */}
            <img
              src="https://images.bewakoof.com/uploads/grid/app/valentine-day-desktop-banner-1549542096.jpg"
              width="100%"
            />
          </div>
          <div>
            <img
              src="https://images.bewakoof.com/uploads/grid/app/12-02-dotd-main-banner-desktop-men-1549967281.jpg"
              width="100%"
              height="auto"
            />
            {/* <h3>2</h3> */}
          </div>
          <div>
            <img
              src="https://images.bewakoof.com/uploads/grid/app/12-02-dotd-main-banner-desktop-men-1549967281.jpg"
              width="100%"
              height="auto"
            />
            {/* <h3>3</h3> */}
          </div>
          <div>
            <img
              src="https://images.bewakoof.com/uploads/grid/app/12-02-RS599-desktop-1549967716.jpg"
              width="100%"
              height="auto"
            />
            {/* <h3>4</h3> */}
          </div>
        </Slider>
      </div>
    );
  }
}

export const EditorsChoice = () => {
  let settings = {
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "60px",
    slidesToShow: 1,
    speed: 500,
    autoplay: true
  };
  console.log(window.innerWidth);
  if (window.innerWidth > 768) {
    console.log(window.innerWidth);
    settings.slidesToShow = 4;
  }

  return (
    <div style={{ marginTop: "50px", textAlign: "center" }}>
      <h4>Editor's Choice</h4>
      <Slider {...settings}>
        <div
          style={{
            backgroundColor: "pink",
            textAlign: "center",
            margin: "0 5px"
          }}
        >
          <img
            src="https://cdn.shopify.com/s/files/1/0065/0788/8755/products/no_way_out_black_360x.png?v=1538949369"
            width="95%"
            height="auto"
          />
          <div>
            <div>Product Title</div>
            <h3>₹ 299</h3>
          </div>
        </div>
        <div style={{ backgroundColor: "pink", textAlign: "center" }}>
          <img
            src="https://cdn.shopify.com/s/files/1/0065/0788/8755/products/no_way_out_black_360x.png?v=1538949369"
            width="95%"
            height="auto"
          />
          <div>
            <div>Product Title</div>
            <h3>₹ 299</h3>
          </div>
        </div>

        <div style={{ backgroundColor: "pink", textAlign: "center" }}>
          <img
            src="https://cdn.shopify.com/s/files/1/0065/0788/8755/products/no_way_out_black_360x.png?v=1538949369"
            width="95%"
            height="auto"
          />
          <div>
            <div>Product Title</div>
            <h3>₹ 299</h3>
          </div>
        </div>

        <div style={{ backgroundColor: "pink", textAlign: "center" }}>
          <img
            src="https://cdn.shopify.com/s/files/1/0065/0788/8755/products/no_way_out_black_360x.png?v=1538949369"
            width="95%"
            height="auto"
          />
          <div>
            <div>Product Title</div>
            <h3>₹ 299</h3>
          </div>
        </div>

        <div style={{ backgroundColor: "pink", textAlign: "center" }}>
          <img
            src="https://cdn.shopify.com/s/files/1/0065/0788/8755/products/no_way_out_black_360x.png?v=1538949369"
            width="95%"
            height="auto"
          />
          <div>
            <div>Product Title</div>
            <h3>₹ 299</h3>
          </div>
        </div>

        <div style={{ backgroundColor: "pink", textAlign: "center" }}>
          <img
            src="https://cdn.shopify.com/s/files/1/0065/0788/8755/products/no_way_out_black_360x.png?v=1538949369"
            width="95%"
            height="auto"
          />
          <div>
            <div>Product Title</div>
            <h3>₹ 299</h3>
          </div>
        </div>
      </Slider>
    </div>
  );
};

// import React, { Component } from "react";
// import "../CSS/slider.css";

// class Slider extends Component {
//   constructor(){
//     super();
//     this.sliderImages = document.querySelectorAll(".slide")
//     this.current = 0;
//   }
// // Clear all images
// reset = () => {
//   for (let i = 0; i < this.sliderImages.length; i++) {
//     this.sliderImages[i].style.display = "none";
//   }
// }

// // Init slider
//  startSlide = () => {
//   this.reset();
//   this.sliderImages[0].style.display = "block";
// }

// // Show prev
//  slideLeft = () => {
//   this.reset();
//   this.sliderImages[this.current - 1].style.display = "block";
//   this.current--;
// }

// // Show next
//  slideRight = () => {
//   this.reset();
//   this.sliderImages[this.current + 1].style.display = "block";
//   this.current++;
// }

// // Left arrow click
// arrowLeft =() => {
//   if (this.current === 0) {
//     this.current = this.sliderImages.length;
//   }
//   this.slideLeft();
// };

// // Right arrow click
// arrowRight = () => {
//   if (this.current === this.sliderImages.length - 1) {
//     this.current = -1;
//   }
//   this.slideRight();
// };
//   render() {
//     return (
//       <div className="wrap">
//         <div  onClick={()=>{this.arrowLeft()}} id="arrow-left" class="arrow" />
//         <div id="slider">
//           <div className="slide slide1">
//             <div className="slide-content">
//               <span>Image One</span>
//             </div>
//           </div>
//           <div className="slide slide2">
//             <div className="slide-content">
//               <span>Image Two</span>
//             </div>
//           </div>
//           <div className="slide slide3">
//             <div className="slide-content">
//               <span>Image Three</span>
//             </div>
//           </div>
//         </div>
//         <div onClick={()=>{this.arrowRight()}} id="arrow-right" class="arrow" />
//       </div>
//     );
//   }
// }

// export default Slider;
