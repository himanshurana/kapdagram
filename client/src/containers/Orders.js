import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getOrders, trackOrder } from "../store/actions/orderActions";
import "../CSS/order.css";

// Loader
import { StageSpinner } from "../assests/stage/index";

class Order extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      orders: [],
      error: false
    };
  }
  componentDidMount() {
    this.props.getOrders(this.props.currentUser.user.id);
  }

  componentWillReceiveProps(nextProps) {
    console.log(":lkjsssssssssssssssssdjsakljasd", this.props.errors.message);
    this.setState({
      orders: nextProps.orders.orders,
      loading: false,
      error: !!this.props.errors.message
    });
  }

  onOrderClick = order_id => {
    this.props.trackOrder(this.props.currentUser.user.id, order_id);
  };

  render() {
    if (this.state.loading) {
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "92vh"
          }}
        >
          <StageSpinner
            size={80}
            color="#686769"
            loading={this.state.loading}
          />
        </div>
      );
    } else {
      let { orders } = this.state;
      if (orders.length) {
        return orders.map((order, index) => {
          return (
            <div className="container orders-container" key={index}>
              <div className="order-content-container">
                <Link className="Link" to={`/orders/${order._id}`}>
                  {order.products.items.map(item => {
                    return item.selectedVariant.selectedSize.map((size, i) => {
                      return (
                        <div>
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "space-between",
                              borderBottom: "1px solid lightgray",
                              padding: "5px 0"
                            }}
                          >
                            <div style={{ display: "flex" }}>
                              <div className="order-img-container">
                                <img
                                  className="order-item-img"
                                  src={
                                    item.selectedVariant.variant.assests.imgs[0]
                                  }
                                  alt="product img"
                                  height="80"
                                  width="80"
                                />
                              </div>
                              <div
                                style={{
                                  display: "flex",
                                  justifyContent: "space-between"
                                }}
                              >
                                <div>
                                  <div className="order-title">
                                    {item.product.title}
                                  </div>
                                  <div className="order-item-color-size-container">
                                    <div style={{ display: "flex" }}>
                                      <div className="color-text">color:</div>
                                      <div className="color-text-value">
                                        {
                                          item.selectedVariant.variant
                                            .attributes.color
                                        }
                                      </div>
                                    </div>
                                    <div className="order-item-color-size-comma">
                                      ,
                                    </div>
                                    <div style={{ display: "flex" }}>
                                      <div className="size-text">size:</div>
                                      <div className="size-text-value">
                                        {size.size}
                                      </div>
                                    </div>
                                  </div>
                                  <div className="order-item-price">{`₹ ${
                                    item.product.prices.sell_price
                                  }`}</div>
                                </div>
                              </div>
                            </div>

                            <div className="item-content-footer-container">
                              <div className="item-content-footer-quantity-container">
                                <div>Qty:</div>
                                <div className="Qty-value-text">{size.qty}</div>
                              </div>
                              <div className="item-content-footer-amount-container">
                                <div>Total:</div>
                                <div className="total-value-text">{`₹ ${
                                  size.price
                                }`}</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    });
                  })}
                </Link>
                <div className="order-item-arrow">Order Details ></div>
              </div>
            </div>
          );
        });
      } else {
        console.log("ms", orders);
        if ("message" in orders) {
          return (
            <div id="empty-order-container">
              <div id="empty-order-title">{orders.message}</div>
              <Link
                to="/signin"
                id="empty-order-continue-shopping-btn"
                className="btn btn-lg"
              >
                Login here!
              </Link>
            </div>
          );
        } else {
          return (
            <div id="empty-order-container">
              <div id="empty-order-title">empty order seciton!</div>
              <Link
                to="/"
                id="empty-order-continue-shopping-btn"
                className="btn btn-lg"
              >
                continue shopping
              </Link>
            </div>
          );
        }
      }
    }
  }
}

function mapStateToProps({ currentUser, orders }) {
  return { currentUser, orders };
}

export default connect(
  mapStateToProps,
  { getOrders, trackOrder }
)(Order);
