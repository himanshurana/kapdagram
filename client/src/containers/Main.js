import React from "react";
import { Switch, Route, withRouter, Redirect } from "react-router-dom";
import { connect } from "react-redux";

// Import Components
import Homepage from "../components/Homepage";
import AuthForm from "../components/AuthForm";
import ProductsList from "./ProductsList";
import Product from "./Product";
import Cart from "./Cart";
import Address from "./Address";
import AddressForm from "../components/AddressForm";
import Summary from "./Summary";
import Payment from "../components/Payment";
import Orders from "./Orders";
import OrderDetails from "./OrderDetails";
import Profile from "./Profile";
import Navbar from "./Navbar";
import ContectUs from "../components/ContectUs";
import PrivacyPolicy from "../components/PrivacyPolicy";
import TermsConditions from "../components/TermsConditions";
import ReturnRefund from "../components/ReturnRefund";
import Footer from "./Footer";

// Actions
import { authUser } from "../store/actions/auth";

const Main = props => {
  const { authUser, errors, currentUser } = props;
  return (
    <div>
      <Navbar currentUser={currentUser} {...props} />
      <Switch>
        <Route
          exact
          path="/"
          render={props => <Homepage currentUser={currentUser} {...props} />}
        />
        <Route
          exact
          path="/signup"
          render={props => {
            return (
              <AuthForm
                errors={errors}
                onAuth={authUser}
                signUp
                buttonText="Signup"
                heading="Join Kapdagram today."
                pushPath="/"
                {...props}
              />
            );
          }}
        />
        <Route
          exact
          path="/checkout_address"
          render={props => {
            console.log("dsfasdfasdfasdfsadf", props);
            if (!currentUser.isAuthenticated) {
              return (
                <AuthForm
                  errors={errors}
                  onAuth={authUser}
                  buttonText="Log in"
                  heading="Welcome Back."
                  pushPath="/checkout_address"
                  {...props}
                />
              );
            }
            return <Address currentUser={currentUser} {...props} />;
          }}
        />
        <Route exact path="/products" component={ProductsList} />
        <Route
          exact
          path="/department/:department_id/products"
          component={ProductsList}
        />
        <Route
          exact
          path="/department/:department_id/category/:category_id"
          component={ProductsList}
        />
        <Route
          exact
          path="/department/:department_id/genre/:genre_id"
          component={ProductsList}
        />
        <Route
          exact
          path="/products/:id/variant/:variant_id"
          render={props => {
            return <Product {...props} />;
          }}
        />
        <Route
          exact
          path="/cart"
          render={props => {
            return <Cart {...props} />;
          }}
        />
        <Route
          exact
          path="/add-new-address"
          currentUser={currentUser}
          component={AddressForm}
        />
        <Route exact path="/checkout_summary" component={Summary} />
        <Route exact path="/checkout_payment" component={Payment} />
        <Route
          exact
          path="/orders"
          render={props => {
            return <Orders {...props} errors={errors} />;
          }}
        />
        <Route exact path="/orders/:order_id" component={OrderDetails} />
        <Route exact path="/profile" component={Profile} />
        <Route exact path="/contect-us" component={ContectUs} />
        <Route exact path="/privacy-policy" component={PrivacyPolicy} />
        <Route exact path="/T&C" component={TermsConditions} />
        <Route exact path="/return-refund" component={ReturnRefund} />
      </Switch>
      <Footer />
    </div>
  );
};

function mapStateToProps(state) {
  return {
    errors: state.errors,
    currentUser: state.currentUser
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    { authUser }
  )(Main)
);
