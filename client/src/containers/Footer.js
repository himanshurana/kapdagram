import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../CSS/footer.css";
const logo = require("../assests/icons/logo.png");
const instagram = require("../assests/icons/instagram.png");
const fb = require("../assests/icons/facebook-logo-button.png");

class Footer extends Component {
  render() {
    return (
      <div id="footer">
        <div className="container">
          <div className="row">
            <div
              className="col-lg-8"
              id="footer-left-content"
            >
              <Link to="/about-us" className="Link" href="#">
                About Us
              </Link>

              <Link to="/contect-us" href="#">
                Contact Us
              </Link>

              <Link to="/T&C" href="#">
                Terms & Conditions{" "}
              </Link>
              <Link to="/privacy-policy" href="#">
                Privacy Policy
              </Link>
              <Link to="/return-policy" href="#">
                Return Policy
              </Link>
            </div>
            <div className="col-lg-4">
              <div id="social-media-container">
                <a href="https://www.facebook.com/kapdagramcom/" target="blank">
                  {" "}
                  <img src={fb} width="30" height="30" />
                </a>
                <a
                  href="https://www.instagram.com/kapdagram/?hl=en"
                  target="blank"
                >
                  {" "}
                  <img src={instagram} width="30" height="30" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

//Connet react comsmallonent with redux store and action creator with the helsmall of react-redux connect funtion
export default Footer;

{
  /* <div id="logo-container">
            <Link to="/" href="#">
              <img src={logo} width="150" height="150" />
            </Link>
          </div>
          <div id="content-container">
            <div>
              <div>CUSTOMER SERVICE</div>
              <Link to="/about-us" href="#">
                About Us
              </Link>
              <Link to="/contect-us" href="#">
                Contact Us
              </Link>
            </div>

            <div id="content-middle-container">
              <div>
              <div>COMPANY</div>
                <div>
                  <Link to="/T&C" href="#">
                    Terms & Conditions{" "}
                  </Link>
                </div>

                <div>
                  <Link to="/privacy-policy" href="#">
                    Privacy Policy
                  </Link>
                </div>
              </div>
              <div id="social-media-container">
                <a href="https://www.facebook.com/kapdagramcom/" target="blank">
                  {" "}
                  <img src={fb} width="30" height="30" />
                </a>
                <a
                  href="https://www.instagram.com/kapdagram/?hl=en"
                  target="blank"
                >
                  {" "}
                  <img src={instagram} width="30" height="30" />
                </a>
              </div>
            </div>
            <div>
              <Link to="/contect-us" href="#">
                Return, Refund,<br/> Exchange & Cancellation Policy
              </Link>
            </div>
          </div> */
}
