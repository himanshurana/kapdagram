import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { addToCart } from "../store/actions/cartActions";
import { getProduct, verifyPincode } from "../store/actions/products";
import { findDOMNode } from "react-dom";
import ReviewList from "./ReviewList";
import favoriteBtn from "../assests/icons/heart.png";
import ReactImageMagnify from "react-image-magnify";

// Loader
import { StageSpinner } from "../assests/stage/index";

// CSS
import "../CSS/productPage.css";

class Proudct extends Component {
  constructor(props) {
    super(props);

    this.state = {
      product: {},
      variant: null,
      selectedVariant: null,
      mainImg: null,
      error: false,
      pincodeVerifyed: false,
      pincodeEntered: false,
      serviceAvailable: true
    };
  }

  componentDidMount() {
    this.props.getProduct(this.props.match.params.id);
  }

  componentWillReceiveProps(nextProps) {
    if ("variants" in nextProps.product) {
      nextProps.product.variants.map(variant => {
        if (variant._id === this.props.match.params.variant_id) {
          return this.setState({ variant, mainImg: variant.assests.imgs[0] });
        }
      });
    }
    return this.setState({
      product: nextProps.product
    });
  }

  renderImgs = () => {
    if (this.state.variant !== null) {
      return this.state.variant.assests.imgs.map((img, index) => {
        return (
          <div
            key={index}
            onClick={() => {
              this.setState({ mainImg: img });
            }}
            style={{
              height: 63,
              width: 63,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              cursor: "pointer"
            }}
          >
            <img src={img} alt="Smiley face" width="53" height="53" />
          </div>
        );
      });
    }
  };

  renderSpecification = () => {
    return this.state.product.specifications.map((item, i) => {
      return (
        <div key={i} className="col">
          <div>{item.name}</div>
          <div>{item.value}</div>
          <hr />
        </div>
      );
    });
  };

  onCartSubmit = () => {
    const { selectedVariant, product } = this.state;

    let item = Object.assign({}, product);
    delete item.category;
    delete item.genre;
    delete item.department;
    delete item.reviews;
    delete item.specifications;
    delete item.variants;

    let history = this.props.history;

    if (selectedVariant) {
      item.selectedVariant = selectedVariant;
    }
    // check if pincode is verifyed
    // console.log(
    //   typeof localStorage.getItem("verifyedPincode"),
    //   localStorage.getItem("verifyedPincode")
    // );
    switch (localStorage.getItem("verifyedPincode")) {
      case null:
        // user not entered pincode or user not verified pincode
        return this.setState({ pincodeEntered: true });
      case "false":
        // our srvices is not available on user selected pincode
        return this.setState({ serviceAvailable: false });
      case "true":
        this.setState({ serviceAvailable: true });
        // our srvices is available on user selected pincode
        if (this.state.selectedVariant !== null) {
          this.props.addToCart(item, history);
          return;
        } else {
          return this.setState({ error: true });
        }
      default:
        return null;
    }
  };

  changeVariant = async (product, variants, variant) => {
    await this.props.history.push(
      `/products/${product._id}/variant/${variant._id}`
    );

    variants.map(variant => {
      if (variant._id === this.props.match.params.variant_id) {
        return this.setState({ variant, mainImg: variant.assests.imgs[0] });
      }
    });
  };

  filterDuplicateColors = (product, variants) => {
    return variants.map((variant, i) => {
      let activeColorBtnClass =
        this.state.variant !== null
          ? this.state.variant.attributes.color === variant.attributes.color
            ? " activeColorBtn"
            : null
          : null;
      let backgroundColorClass = ` ${variant.attributes.color} `;

      return (
        <Link
          key={i}
          to={`/products/${product._id}/variant/${variant._id}`}
          style={{
            backgroundColor: ` ${variant.attributes.color} `
          }}
          onClick={() => {
            this.changeVariant(product, variants, variant);
          }}
          className={"colorBtn " + backgroundColorClass + activeColorBtnClass}
        />
      );
    });
  };

  // filter sizes depends on user selected item color
  filterSize = variants => {
    let { variant } = this.state;
    // make a variable for storing the user selected variant
    // let userSelectedVariantData = null;
    // return variants.map((variant) => {
    // console.log("variant", variant)
    return variant.attributes.size.map((size, index) => {
      let sizeBtnClass =
        this.state.selectedVariant !== null
          ? this.state.selectedVariant.selectedSize === size
            ? " sizeBackgroundColor"
            : null
          : null;
      return (
        <div
          key={index}
          className={"sizeBtn " + sizeBtnClass}
          onClick={() => {
            this.setState({
              selectedVariant: {
                variant,
                selectedSize: size
              },
              error: false
            });
          }}
        >
          {size}
        </div>
      );
      // });
      // if (userSelectedVariantData !== null) {
      //   if (
      //     userSelectedVariantData.attributes.color ===
      //     arr[index].attributes.color
      //   ) {
      //     let sizeBtnClass =
      //       this.state.selectedVariant !== null
      //         ? this.state.selectedVariant.attributes.size ===
      //           variant.attributes.size
      //           ? " sizeBackgroundColor"
      //           : null
      //         : null;
      //     // In this scope we only have access to the variants which have same color value of userSelectedVariant.
      //     return (
      //       <div
      //         key={index}
      //         className={"sizeBtn " + sizeBtnClass}
      //         onClick={() => {
      //           this.setState({
      //             selectedVariant: variant,
      //             error: false
      //           });
      //         }}
      //       >
      //         {variant.attributes.size}
      //       </div>
      //     );
      //   }
      // }
    });
    // });
  };

  veryfyPincode = () => {
    localStorage.removeItem("verifyedPincode");
    let pincode = findDOMNode(this.refs.pincode).value;
    localStorage.setItem("pincode", pincode);
    if (pincode) {
      this.setState({ pincodeEntered: false });
    }
    this.props.verifyPincode(pincode);
  };

  renderServiceAvailabeText = () => {
    if (!this.state.serviceAvailable) {
      return (
        <div style={{ color: "red" }}>
          our services is not availabe in your area please try another pincode
        </div>
      );
    }
  };

  render() {
    const { product } = this.state;
    if ("title" in product) {
      return (
        <div className="container">
          <div className="row" style={{ marginTop: "50px" }}>
            <div className="col-md-4" style={{}}>
              <ReactImageMagnify
                enlargedImageContainerStyle={{ zIndex: 1 }}
                {...{
                  smallImage: {
                    alt: "Wristwatch by Ted Baker London",
                    isFluidWidth: true,
                    src: this.state.mainImg
                  },
                  largeImage: {
                    src: this.state.mainImg,
                    width: 1200,
                    height: 1800
                  }
                }}
              />
              <div
                style={{
                  display: "flex",
                  margin: "5px 0",
                  justifyContent: "center",
                  border: "1px solid whitesmoke",
                  boxShadow: "2.5px 2.5px 1px whitesmoke",
                  overflowX: "scroll"
                }}
              >
                {this.renderImgs()}
              </div>
            </div>
            <div className="col-md-8">
              <div className="product-title">{product.title}</div>
              <div className="product-price">₹{product.prices.sell_price}</div>
              {/* <div style={{ display: "flex", alignItems: "flex-end" }}>
                <div
                  style={{
                    marginLeft: 5,
                    fontSize: "14px",
                    position: "relative"
                  }}
                >
                  <div>{product.prices.mrp}</div>
                  <div
                    style={{
                      borderBottom: "2px solid red",
                      position: "absolute",
                      top: 10,
                      width: 22
                    }}
                  />
                </div>
                <div
                  style={{ fontSize: "13px", marginLeft: 5, color: "#388e3c" }}
                >
                  <div>{product.prices.discount}% off</div>
                </div>
              </div> */}
              <div>
                <span style={{ fontSize: 12 }}>color</span>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center"
                  }}
                >
                  {this.filterDuplicateColors(product, product.variants)}
                </div>
              </div>

              <div
                style={{ margin: "10px 0" }}
                className={this.state.error ? "error" : undefined}
              >
                <span style={{ fontSize: 12, marginRight: 10 }}>size</span>
                <div style={{ display: "flex" }}>
                  {this.filterSize(product.variants)}
                </div>
              </div>

              <div style={{ margin: "10px 0" }}>
                <small>check our delivery service in your area</small>
                <div
                  className={`pincodeInputContainer ${
                    this.state.pincodeEntered ? "error" : ""
                  }`}
                >
                  <input
                    type="text"
                    ref="pincode"
                    placeholder={localStorage.getItem("pincode")}
                    className={`${this.state.pincodeEntered ? "error" : ""}`}
                    style={{ border: "none", outline: "none" }}
                  />

                  <div
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      this.veryfyPincode();
                    }}
                  >
                    check
                  </div>
                </div>
              </div>
              <div
                className="btn-toolbar"
                role="toolbar"
                aria-label="Toolbar with button groups"
              >
                {/* <div className="btn-group mr-2">
                  <Link to="/favorite" onClick={() => {}}>
                    <button
                      type="button"
                      className="btn btn-lg btn-outline-dark"
                    >
                      <img
                        src={favoriteBtn}
                        alt="Smiley face"
                        width="30"
                        height="25"
                      />
                    </button>
                  </Link>
                </div> */}

                <div className="btn-group">
                  <button
                    // disabled={!!this.state.serviceAvailable}
                    type="button"
                    className="btn btn-lg cartBtn"
                    onClick={() => this.onCartSubmit()}
                  >
                    ADD TO CART
                  </button>
                </div>
                {this.renderServiceAvailabeText()}
              </div>
              <div id="product-description-container">
                <p className="product-description-title">PRODUCT DETAILS</p>
                <p>{product.description}</p>
                <div>
                  <p className="product-description-title">Specifications</p>
                  <div className="row">{this.renderSpecification()}</div>
                </div>
              </div>
            </div>
            {/* <ReviewList product_id={product._id} /> */}
          </div>
        </div>
      );
    } else {
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "92vh"
          }}
        >
          <StageSpinner size={80} color="#686769" loading={true} />
        </div>
      );
    }
  }
}

function mapStateToProps(state) {
  return { product: state.product };
}

export default connect(
  mapStateToProps,
  { addToCart, getProduct, verifyPincode }
)(Proudct);
