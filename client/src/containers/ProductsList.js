import React, { Component } from "react";
import { connect } from "react-redux";
// REDUX
import { bindActionCreators } from "redux";
import { getCategories } from "../store/actions/categoriesActions";
import { getProducts } from "../store/actions/products";
// Loader
import { StageSpinner } from "../assests/stage/index";
import ProductsListItem from "../components/ProductsListItem";
// CSS
import "../CSS/productsPage.css";

class Proudcts extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      products: [],
      fetchedCategories: [],
      filteredProducts: []
    };
  }

  componentDidMount() {
    this.props.getProducts();
    this.props.getCategories();
  }

  componentWillReceiveProps(nextProps) {
    setTimeout(() => {
      this.setState({
        products: nextProps.products.products,
        filteredProducts: nextProps.products.products,
        fetchedCategories: nextProps.categories.categories,
        loading: false
      });
      this.filterProducts();
    }, 1000);
  }

  filterProducts = async () => {
    // getting params is async code
    let department_id = await this.props.match.params.department_id;
    let category_id = await this.props.match.params.category_id;
    let genre_id = await this.props.match.params.genre_id;
    console.log("chabnge", department_id);
    // use filterProducts variable for saving the filtered products
    let filteredProducts = [];
    this.state.products.map(product => {
      // make sure prduct has category id
      if (product.department !== undefined) {
        //campare the product category to category
        if (product.department === department_id) {
          if (!!category_id) {
            if (product.category === category_id) {
              // update the filteredProducts state variable
              filteredProducts.push(product);
              return this.setState({ filteredProducts });
            } else {
              // If any product not present in category then run this block of code
              return this.setState({ filteredProducts });
            }
          }
          if (!!genre_id) {
            if (product.genre === genre_id) {
              // update the filteredProducts state variable
              filteredProducts.push(product);
              return this.setState({ filteredProducts });
            } else {
              // If any product not present in genre then run this block of code
              return this.setState({ filteredProducts });
            }
          }
          // update the filteredProducts state variable
          filteredProducts.push(product);
          return this.setState({ filteredProducts });
        }
      }
    });
  };
  render() {
    if (this.state.loading) {
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            minHeight: "75vh"
          }}
        >
          <StageSpinner
            size={80}
            color="#686769"
            loading={this.state.loading}
          />
        </div>
      );
    } else {
      return (
        <div className="container" style={{ marginTop: 50,  minHeight: "75vh" }}>
          <div className="row">
            <ProductsListItem fetchedProducts={this.state.filteredProducts} />
          </div>
        </div>
      );
    }
  }
}

function mapStateToProps({ products, categories }) {
  return { products, categories };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getProducts,
      getCategories
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Proudcts);
