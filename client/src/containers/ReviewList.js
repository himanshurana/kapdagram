import React, { Component } from "react";
import { connect } from "react-redux";
import { getReviews, removeReview } from "../store/actions/reviewActions";
import ReviewItem from "../components/ReviewItem";

class ReviewList extends Component {
  componentDidMount() {
    this.props.getReviews(this.props.product_id);
  }
  render() {
    const { messages, removeReview, currentUser } = this.props;
    let messageList = messages.map(m => (
      <ReviewItem
        key={m._id}
        date={m.createAt}
        text={m.text}
        username={m.user.username}
        profileImageUrl={m.user.profileImageUrl}
        removeReview={removeReview.bind(this, m.user._id, m._id)}
        isCorrectUser={currentUser === m.user._id}
      />
    ));
    return (
      <div className="row col-sm-8">
        <div className="offset-1 col-sm-10">
          <ul className="list-group" id="messages">
            {messageList}
          </ul>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    messages: state.messages,
    currentUser: state.currentUser.user.id
  };
}

export default connect(mapStateToProps, { getReviews, removeReview })(
  ReviewList
);
