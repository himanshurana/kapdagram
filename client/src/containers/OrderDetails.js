import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { trackOrder, cancelOrder } from "../store/actions/orderActions";
import "../CSS/orderDetails.css";

// Loader
import { StageSpinner } from "../assests/stage/index";

class OrderDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      order: {},
      statusColor: ""
    };
  }
  componentDidMount() {
    this.props.trackOrder(
      this.props.currentUser.user.id,
      this.props.match.params.order_id
    );
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ order: nextProps.orders.orders, loading: false });
    switch (this.state.order.status) {
      case "Processing Stock":
        this.setState({ statusColor: "green" });
      case "Ready For Packing":
        this.setState({ statusColor: "green" });
      case "Ready To Deliver":
        this.setState({ statusColor: "green" });
      case "Delivery in Progress":
        this.setState({ statusColor: "green" });
      case "Delivered":
        this.setState({ statusColor: "green" });
      case "Received":
        this.setState({ statusColor: "green" });
      case "Not Delivered":
        this.setState({ statusColor: "red" });
    }
  }

  render() {
    let { order } = this.state;
    let { status } = order;
    if (this.state.loading) {
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "92vh"
          }}
        >
          <StageSpinner
            size={80}
            color="#686769"
            loading={this.state.loading}
          />
        </div>
      );
    } else {
      if ("status" in order) {
        return (
          <div className="container" style={{ marginTop: 25 }}>
            <div id="status-cancel-container">
              <div>
                <small>Status:</small>{" "}
                <span
                  style={{ color: this.state.statusColor }}
                  className="order-details-status-value"
                >
                  {status}
                </span>
              </div>
              {status === "Processing Stock" ||
              status === "Ready For Packing" ||
              status === "Ready To Deliver" ? (
                <button
                  className="btn btn-danger"
                  id="cancel-btn"
                  onClick={() =>
                    this.props.cancelOrder(
                      this.props.currentUser.user.id,
                      this.props.match.params.order_id
                    )
                  }
                >
                  cancel
                </button>
              ) : null}
            </div>
            <div className="items-container">
              {order.products.items.map(item => {
                // console.log("item", item);
                return item.selectedVariant.selectedSize.map((size, i) => {
                  return (
                    <div>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                          borderBottom: "1px solid lightgray",
                          padding: "5px 0"
                        }}
                      >
                        <div style={{ display: "flex" }}>
                          <div className="order-img-container">
                            <img
                              src={item.selectedVariant.variant.assests.imgs[0]}
                              alt="product img"
                              height="80"
                              width="80"
                            />
                          </div>
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "space-between"
                            }}
                          >
                            <div>
                              <div className="order-title">
                                {item.product.title}
                              </div>
                              <div className="order-item-color-size-container">
                                <div style={{ display: "flex" }}>
                                  <div className="color-text">color:</div>
                                  <div className="color-text-value">
                                    {
                                      item.selectedVariant.variant.attributes
                                        .color
                                    }
                                  </div>
                                </div>
                                <div className="order-item-color-size-comma">
                                  ,
                                </div>
                                <div style={{ display: "flex" }}>
                                  <div className="size-text">size:</div>
                                  <div className="size-text-value">
                                    {size.size}
                                  </div>
                                </div>
                              </div>
                              <div className="order-item-price">{`₹ ${
                                item.product.prices.sell_price
                              }`}</div>
                            </div>
                          </div>
                        </div>

                        <div className="item-content-footer-container">
                          <div className="item-content-footer-quantity-container">
                            <div>Qty:</div>
                            <div className="Qty-value-text">{size.qty}</div>
                          </div>
                          <div className="item-content-footer-amount-container">
                            <div>Total:</div>
                            <div className="total-value-text">{`₹ ${
                              size.price
                            }`}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                });
              })}
            </div>
            <div className="row">
              <div className="col-sm-6 address-container">
                <p className="delivery-add-title">Delivery Address</p>
                <div>
                  <div className="delivery-customer-name">
                    {order.shipping.customer_name}
                  </div>
                  <div className="delivery-customer-add">{`${
                    order.shipping.landmark
                  } ${order.shipping.customer_address_first_line}, ${
                    order.shipping.customer_address_second_line
                  }, - ${order.shipping.pincode}`}</div>
                  <br />
                  <div>{`Mobile: ${order.shipping.mobile}`}</div>
                </div>
              </div>

              <div className="col-sm-6 payment-summary-container">
                <p className="payment-summary-title">Payment Summary</p>
                <div className="payment-summary-price-container">
                  <div className="payment-summary-content-text">Price</div>
                  <div className="payment-summary-content-text">
                    ₹{order.products.totalPrice}
                  </div>
                </div>
                <div className="payment-summary-shipping-fee-container">
                  <div className="payment-summary-content-text">
                    Shipping Fee
                  </div>
                  <div className="payment-summary-content-text">₹ 0</div>
                </div>
                <div className="payment-summary-order-total-container">
                  <div className="payment-summary-order-total-title">
                    Order Total
                  </div>
                  <div className="payment-summary-order-total-value">
                    ₹{order.products.totalPrice}
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      } else {
        return (
          <div id="empty-order-container">
            <div id="empty-order-title">empty order seciton!</div>
            <Link
              to="/"
              id="empty-order-continue-shopping-btn"
              className="btn btn-lg"
            >
              continue shopping
            </Link>
          </div>
        );
      }
    }
  }
}

function mapStateToProps({ currentUser, orders }) {
  return { currentUser, orders };
}

export default connect(
  mapStateToProps,
  { trackOrder, cancelOrder }
)(OrderDetails);
