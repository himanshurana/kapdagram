import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getDepartments } from "../store/actions/departmentActions";
import { getCategories } from "../store/actions/categoriesActions";
import { getGenres } from "../store/actions/genreActions";
import { logout } from "../store/actions/auth";

// CSS FILE
import "../CSS/navbar.css";
// import Logo from "../images/warbler-logo.png";
const cartBtn = require("../assests/icons/shopping-bag.png");
const profileBtn = require("../assests/icons/user.png");
const logo = require("../assests/icons/logo1.png");

class Navbar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      departments: [],
      genres: [],
      categories: []
    };
  }
  componentDidMount() {
    this.props.getDepartments();
    this.props.getCategories();
    this.props.getGenres();
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      departments: nextProps.departments.departments,
      genres: nextProps.genres.genres,
      categories: nextProps.categories.categories
    });
  }
  renderDepartment = () => {
    return this.state.departments.map((department, index) => {
      let lDepartment = department.title.toLowerCase();
      // console.log(lDepartment)
      return (
        <li key={index} className="nav-item active dropdown">
          <Link
            className="nav-link dropbtn"
            to={`/department/${department._id}/products`}
          >
            <span>{department.title.toUpperCase()}</span>
          </Link>
          {/* {lDepartment === "men" || lDepartment === "women" ? ( */}
          <div className="dropdown-content">
            <div id="genres-content">
              <small>Genres</small>
              <div id="genres-ul">
                {this.state.genres.map((genre, i) => {
                  return (
                    <Link
                      key={i}
                      to={`/department/${
                        department._id
                      }/genre/${genre.title.toLowerCase()}`}
                      className="genres-text"
                    >
                      {genre.title}
                    </Link>
                  );
                })}
              </div>
            </div>
            <div id="categories-content">
              <small>Categories</small>
              <div id="category-ul">
                {department.categories.map((category, i) => {
                  return (
                    <Link
                      key={i}
                      to={`/department/${department._id}/category/${
                        category._id
                      }`}
                      className="category-text"
                    >
                      {category.title}
                    </Link>
                  );
                })}
              </div>
            </div>
          </div>
          {/* ) : lDepartment === "accessories" ? (
            <div className="dropdown-content">
              <small>Categories</small>
              <div id="genres-ul">
                <Link to="#" className="genres-text">
                  white mugs
                </Link>
              </div>
            </div>
          ) : null} */}
        </li>
      );
    });
  };

  burgerClick = () => {
    const burger = document.querySelector(".burger");
    const innerBurger = document.querySelector(".inner-burger");
    const nav = document.querySelector(".nav-links");
    const navLinks = document.querySelectorAll(".nav-links li");

    nav.classList.toggle("nav-active");
    //animate links
    navLinks.forEach((link, index) => {
      if (link.style.animation) {
        link.style.animation = "";
      } else {
        link.style.animation = `navLinkFade 0.5s ease forwards ${index / 7 +
          0.3}s`;
      }
    });
    //burger animation
    burger.classList.toggle("toggle");
    innerBurger.classList.toggle("toggle");
  };

  logout = e => {
    e.preventDefault();
    this.props.logout();
  };

  render() {
    return (
      <div style={{ boxShadow: "0.5px 2.5px whitesmoke" }}>
        <header className="container">
          <nav>
            <div className="logo">
              <Link to="/" href="#">
                <div id="navbar-brand-title">KAPDAGRAM</div>
                {/* <img src={logo} width="250" height="200" /> */}
              </Link>
            </div>
            <ul className="nav-links">
              <div id="burger-container">
                <div
                  className="inner-burger"
                  onClick={() => this.burgerClick()}
                >
                  <div className="line1" />
                  <div className="line2" />
                  <div className="line3" />
                </div>
              </div>
              {this.renderDepartment()}
            </ul>
            <div style={{ display: "flex" }}>
              <div style={{ display: "flex", alignItems: "center" }}>
                {this.props.currentUser.isAuthenticated ? (
                  <div className="nav-item active dropdown">
                    <Link to="/profile" className="profile-btn-container dropbtn">
                      <img
                        src={profileBtn}
                        alt="user button icon"
                        height="24"
                        width="24"
                        style={{ cursor: "pointer" }}
                      />
                    </Link>

                    <div className="profile-dropdown-content">
                      <div>
                        <div>
                          <Link className="Link" to="orders">
                            orders
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                ) : (
                  <div>
                    <Link to="/signin" style={{ marginRight: 15 }}>
                      <img
                        src={profileBtn}
                        alt="user button icon"
                        height="24"
                        width="24"
                        style={{ cursor: "pointer" }}
                      />
                    </Link>
                  </div>
                )}
                <Link to="/cart" className="cart-btn-container">
                  <img
                    src={cartBtn}
                    alt="cart button icon"
                    height="24"
                    width="24"
                    style={{ cursor: "pointer" }}
                  />
                </Link>
              </div>
              <div id="burger-container">
                <div className="burger" onClick={() => this.burgerClick()}>
                  <div className="line1" />
                  <div className="line2" />
                  <div className="line3" />
                </div>
              </div>
            </div>
          </nav>
        </header>
      </div>
    );
  }
}

function mapStateToProps({ currentUser, departments, genres, categories }) {
  return {
    currentUser,
    departments,
    genres,
    categories
  };
}

export default connect(
  mapStateToProps,
  { getDepartments, getCategories, getGenres, logout }
)(Navbar);
