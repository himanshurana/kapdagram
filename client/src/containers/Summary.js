import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import {
  getCart,
  reduce,
  addToCart,
  onRemove
} from "../store/actions/cartActions";
import { addPaymentModeToCheckoutObect } from "../store/actions/checkoutActions";
import { postOrder } from "../store/actions/orderActions";
// CSS
import "../CSS/summary.css";

// Loader
import { StageSpinner } from "../assests/stage/index";

class Summary extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      cart: {},
      shippingAddress: {},
      payement_COD: false
    };
  }
  componentDidMount() {
    this.props.getCart();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      cart: nextProps.cart.cart,
      shippingAddress: nextProps.checkoutObject.shippingAddress,
      loading: false
    });
  }

  render() {
    if (this.state.loading) {
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "92vh"
          }}
        >
          <StageSpinner
            size={80}
            color="#686769"
            loading={this.state.loading}
          />
        </div>
      );
    } else {
      const { shippingAddress, cart } = this.state;
      console.log("ff", cart);
      if (cart.products !== null) {
        if ("mobile" in shippingAddress) {
          return (
            <div className="container">
              <div className="row" id="summary-page-container">
                <div className="col-lg-8">
                  {cart.products.map((item, i) => {
                    console.log(item);
                    return item.selectedVariant.selectedSize.map((size, i) => {
                      return (
                        <div
                          className="row"
                          key={i}
                          style={{
                            boxShadow: "2px 2px 0.5px whitesmoke",
                            border: "2px solid whitesmoke",
                            margin: "5px 0 0 0",
                            padding: "10px 0",
                            display: "flex",
                            alignItems: "center"
                          }}
                        >
                          <div className="col col-sm-3">
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "column",
                                justifyContent: "center",
                                alignItems: "center"
                              }}
                            >
                              <img
                                src={
                                  item.selectedVariant.variant.assests.imgs[0]
                                }
                                alt="product"
                                height="auto"
                                width="50%"
                              />
                              <p>{item.product.title}</p>
                            </div>
                          </div>
                          <div
                            className="col col-sm-9"
                            style={{
                              display: "flex",
                              justifyContent: "space-around",
                              flexWrap: "wrap"
                            }}
                          >
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "column",
                                justifyContent: "center",
                                alignItems: "center"
                              }}
                            >
                              <small style={{ color: "gray" }}>size</small>
                              <h6>{size.size}</h6>
                            </div>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "column",
                                justifyContent: "center",
                                alignItems: "center"
                              }}
                            >
                              <small style={{ color: "gray" }}>color</small>
                              <h6>
                                {item.selectedVariant.variant.attributes.color}
                              </h6>
                            </div>

                            <div
                              style={{
                                display: "flex",
                                flexDirection: "column",
                                justifyContent: "center",
                                alignItems: "center"
                              }}
                            >
                              <small style={{ color: "gray" }}>amount</small>
                              <h6>{size.qty}</h6>
                            </div>

                            <div
                              style={{
                                display: "flex",
                                flexDirection: "column",
                                justifyContent: "center",
                                alignItems: "center"
                              }}
                            >
                              <small style={{ color: "gray" }}>amount</small>
                              <h6>{size.price}</h6>
                            </div>
                          </div>
                        </div>
                      );
                    });
                  })}
                  <div
                    style={{
                      boxShadow: "2px 2px 0.5px whitesmoke",
                      border: "2px solid whitesmoke",
                      margin: "5px 0",
                      padding: "10px"
                    }}
                  >
                    <small>Delivery Address</small>
                    <div>
                      <div>{shippingAddress.customer_name}</div>
                      <div>{`${shippingAddress.landmark} ${
                        shippingAddress.customer_address_first_line
                      }, ${shippingAddress.customer_address_second_line}, - ${
                        shippingAddress.pincode
                      }`}</div>
                    </div>
                    <Link to="/checkout_address">
                      <p style={{ color: "#67CECD", fontSize: "1em" }}>
                        Change
                      </p>
                    </Link>
                  </div>
                </div>
                <div className="col-lg-4">
                  <div
                    style={{
                      boxShadow: "2px 2px 0.5px whitesmoke",
                      border: "2px solid whitesmoke",
                      padding: "10px"
                    }}
                  >
                    <div className="row">
                      <div className="col">
                        <small style={{ color: "gray" }}>Payable Amount</small>
                        <div>Rs. {cart.totalPrice}</div>
                      </div>
                      <div className="col">
                        <small style={{ color: "gray" }}>Total Quantity</small>
                        <div className="">{cart.totalQty}</div>
                      </div>
                    </div>
                    <div style={{ marginTop: 20 }}>
                      <div
                        style={{
                          color: "gray",
                          borderBottom: "1px solid lightgray"
                        }}
                      >
                        payment mode
                      </div>
                      <div style={{ marginTop: 10 }}>
                        <input
                          type="radio"
                          id="huey"
                          name="drone"
                          value="huey"
                          onChange={() => {
                            this.setState({ payement_COD: true });
                            this.props.addPaymentModeToCheckoutObect({
                              method: "COD"
                            });
                          }}
                        />
                        <label>COD</label>
                      </div>
                      {this.state.payement_COD ? (
                        <div style={{ marginTop: 20 }}>
                          <button
                            onClick={() => {
                              this.props.postOrder(
                                this.props.currentUser.user.id,
                                this.props.checkoutObject,
                                this.props.history
                              );
                            }}
                            className="btn"
                            style={{ backgroundColor: "black", color: "white" }}
                          >
                            CONFIRM ORDER
                          </button>
                        </div>
                      ) : null}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
        } else {
          return (
            <div id="empty-order-container">
              <div id="empty-order-title">Select Address First!</div>
              <Link
                to="/checkout_address"
                id="empty-order-continue-shopping-btn"
                className="btn btn-lg"
              >
                Select Address
              </Link>
            </div>
          );
        }
      } else {
        return (
          <div id="empty-order-container">
            <div id="empty-order-title">Your Cart is empty!</div>
            <Link
              to="/"
              id="empty-order-continue-shopping-btn"
              className="btn btn-lg"
            >
              Continue Shopping
            </Link>
          </div>
        );
      }
    }
  }
}
const mapStateToProps = ({ currentUser, checkoutObject, cart }) => {
  return { currentUser, checkoutObject, cart };
};
export default connect(
  mapStateToProps,
  {
    getCart,
    reduce,
    addToCart,
    onRemove,
    addPaymentModeToCheckoutObect,
    postOrder
  }
)(Summary);
