import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { logout } from "../store/actions/auth";

// CSS
import "../CSS/profilePage.css";

class Profile extends Component {
  logout = e => {
    e.preventDefault();
    this.props.logout();
  };

  render() {
    if (this.props.currentUser.isAuthenticated) {
      return (
        <div id="profile-page-container">
          <div id="profile-page-content-container">
            <p id="profile-page-email-text">
              {this.props.currentUser.user.email}
            </p>
            <div id="profile-page-btns-container">
              <Link to="/orders" className="btn" id="profile-page-orderBtn">
                Orders
              </Link>
              <button className="btn" id="logoutBtn" onClick={this.logout}>
                Log out
              </button>
            </div>
          </div>
        </div>
      );
    } else {
      return <div> {this.props.history.push("/")}</div>;
    }
  }
}

function mapStateToProps({ currentUser }) {
  return { currentUser };
}

export default connect(
  mapStateToProps,
  { logout }
)(Profile);
