import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getReviews } from '../redux/actions/reviewActions';

class Review extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // reviews: [],
            filteredReviews: []
        }
    }

    componentDidMount() {
        this.props.getReviews();
    }

    componentWillReceiveProps(nextProps) {
        let filterReviews = [];
        let product = this.props.product;
        nextProps.reviews.reviews.map(review => {
            // campare product id to review product id
            if (product._id === review.product) {
                filterReviews.push(review)
            };
        });
        // update the filteredProducts state variable
        this.setState({ filteredReviews: filterReviews });
    }

    review = () => {
        return this.state.filteredReviews.map(review => {
            console.log(review)
            return <div><p>{review.text}</p><p>{review.user.displayName}</p></div>
        });
    };

    render() {
        return (
            <div className="container">
                {this.review()}
            </div>
        );
    }
}

function mapStateToProps({ SelctedProductReducer, reviews }) {
    return { SelctedProductReducer, reviews };
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getReviews }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Review);
