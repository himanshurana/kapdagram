import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
//IMPORT ACTIONS
import {
  getCart,
  reduce,
  addToCart,
  onRemove
} from "../store/actions/cartActions";

// Loader
import { StageSpinner } from "../assests/stage/index";
import CartList from "../components/CartList";

// CSS
import "../CSS/cart.css";

class Cart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      cart: {}
    };
  }
  componentDidMount() {
    this.props.getCart();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ cart: nextProps.cart.cart, loading: false });
  }

  renderEmpty = () => {
    return (
      <div id="empty-cart-container">
        <div id="empty-cart-title">You cart is empty!</div>
        <Link
          to="/"
          id="empty-cart-continue-shopping-btn"
          className="btn btn-lg"
        >
          continue shopping
        </Link>
      </div>
    );
  };

  renderCart = cart => {
    return (
      <div header="Cart" className="container" style={{ marginTop: 50 }}>
        <CartList
          cart={cart}
          reduce={this.props.reduce}
          addToCart={this.props.addToCart}
          onRemove={this.props.onRemove}
        />
        <div style={{ marginTop: 20 }}>
          <div className="row">
            <div className="col-sm-8">
              <div className="row">
                <div className="col">
                  <small style={{ color: "gray" }}>Payable Amount</small>
                  <div className="">Rs. {cart.totalPrice}</div>
                </div>
                <div className="col">
                  <small className="" style={{ color: "gray" }}>
                    Total Quantity
                  </small>
                  <div className="">{cart.totalQty}</div>
                </div>
              </div>
            </div>
            <div
              className="col-sm-4"
              style={{ display: "flex", justifyContent: "flex-end" }}
            >
              <Link to="/checkout_address">
                <button
                  // onClick={() => this.onCheckout(cart)}
                  className="btn"
                  style={{
                    backgroundColor: "black",
                    color: "white",
                    fontWeight: "bold"
                  }}
                >
                  CHECKOUT
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  };

  render() {
    if (this.state.loading) {
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "92vh"
          }}
        >
          <StageSpinner
            size={80}
            color="#686769"
            loading={this.state.loading}
          />
        </div>
      );
    } else {
      let { cart } = this.state;
      if ("products" in cart) {
        if (cart.totalQty) {
          return this.renderCart(cart);
        }
      }
      return this.renderEmpty();
    }
  }
}

function mapStateToProps({ cart }) {
  return { cart };
}

export default connect(
  mapStateToProps,
  {
    getCart,
    reduce,
    addToCart,
    onRemove
  }
)(Cart);
