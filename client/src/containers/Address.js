import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import AddressList from "../components/AddressList";
import { getAddress, removeAddress } from "../store/actions/addressActions";
import { addAddressToCheckoutObect } from "../store/actions/checkoutActions";

// Loader
import { StageSpinner } from "../assests/stage/index";

class Address extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      addressArr: null
    };
  }

  componentDidMount() {
    this.props.getAddress(this.props.currentUser.user.id);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ addressArr: nextProps.address.address, loading: false });
  }

  render() {
    // const addressArr = this.props.address.address;
    const {
      history,
      currentUser,
      removeAddress,
      addAddressToCheckoutObect
    } = this.props;
    if (this.state.loading) {
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "92vh"
          }}
        >
          <StageSpinner
            size={80}
            color="#686769"
            loading={this.state.loading}
          />
        </div>
      );
    } else {
      return (
        <div className="container">
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
              margin: "20px 0 10px 0",
            }}
          >
            <Link to="/add-new-address">
              <button
                className="btn"
                style={{ backgroundColor: "black", color: "white" }}
              >
                add new address
              </button>
            </Link>
          </div>
          <AddressList
            history={history}
            currentUser={currentUser}
            addressArr={this.state.addressArr}
            removeAddress={removeAddress}
            addAddressToCheckoutObect={addAddressToCheckoutObect}
          />
        </div>
      );
    }
  }
}
const mapStateToProps = ({ address, currentUser }) => {
  return { address, currentUser };
};
export default connect(
  mapStateToProps,
  { getAddress, removeAddress, addAddressToCheckoutObect }
)(Address);
