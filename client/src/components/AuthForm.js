import React, { Component } from "react";
import { Link } from "react-router-dom";

const initialState = {
  email: "",
  password: "",
  reEnterPassword: "",
  passwordError: "",
  reEnterPasswordError: "",
  emailError: "",
  passwordError: "",
  passwaordMatchedError: ""
};

class AuthForm extends Component {
  state = initialState;

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  validate = authType => {
    let emailError = "";
    let passwordError = "";
    let reEnterPasswordError = "";
    let passwaordMatchedError = "";
    if (!this.state.email.includes("@")) {
      emailError = "invalid email";
    }
    if (!this.state.password) {
      passwordError = "please enter the passwaord";
    }
    // if user trying to signup then also validate the re-enter passward and both passwaord match query
    if (authType === "signup") {
      if (!this.state.reEnterPassword) {
        reEnterPasswordError = "please enter the re-enter passwaord";
      }
      let isBothPasswordMatched =
        this.state.password === this.state.reEnterPassword;

      if (!isBothPasswordMatched) {
        passwaordMatchedError = "password mismatch";
      }
    }

    if (
      emailError ||
      passwordError ||
      reEnterPasswordError ||
      passwaordMatchedError
    ) {
      this.setState({
        emailError,
        reEnterPasswordError,
        passwordError,
        passwaordMatchedError
      });
      return false;
    }

    return true;
  };

  handleSubmit = e => {
    e.preventDefault();

    let { email, password } = this.state;
    const { pushPath } = this.props;
    const authType = this.props.signUp ? "signup" : "signin";
    const isValid = this.validate(authType);
    if (isValid) {
      this.props
        .onAuth(authType, { email, password })
        .then(() => {
          this.props.history.push(pushPath);
        })
        .catch(() => {
          return;
        });

      // clear form
      this.setState(initialState);
    }
  };

  render() {
    const { email, password, reEnterPassword } = this.state;
    const { buttonText, errors } = this.props;

    return (
      <div className="container" style={{ marginTop: 50 }}>
        <div className="row">
          <div className=" col-md-3 col-lg-4" />
          <div className=" col-md-6 col-lg-4">
            <div
              style={{
                border: "1px solid whitesmoke",
                padding: 20,
                boxShadow: "2.5px 5px 5px whitesmoke"
              }}
            >
              <form onSubmit={this.handleSubmit}>
                <h4>{buttonText}</h4>
                <div
                  style={{ display: "flex", justifyContent: "space-around" }}
                />
                {errors.message && (
                  <div className="alert alert-danger">{errors.message}</div>
                )}
                <div className={this.state.emptyEmail ? "error" : null}>
                  <small>E-mail</small>
                  <input
                    autoComplete="off"
                    className="form-control"
                    id="email"
                    name="email"
                    onChange={this.handleChange}
                    type="text"
                    value={email}
                  />
                  <div style={{ fontSize: 12, color: "red" }}>
                    {this.state.emailError}
                  </div>
                </div>
                <div
                  className={this.state.emptyReEnterPassword ? "error" : null}
                >
                  <small>Password</small>
                  <input
                    autoComplete="off"
                    className="form-control"
                    id="password"
                    name="password"
                    onChange={this.handleChange}
                    type="password"
                    value={password}
                  />
                  <div style={{ fontSize: 12, color: "red" }}>
                    {this.state.passwordError}
                  </div>
                </div>
                {buttonText === "Signup" ? (
                  <div>
                    <small>re-enter password</small>
                    <div style={{ fontSize: 12, color: "red" }}>
                      {this.state.passwaordMatchedError}
                    </div>
                    <input
                      autoComplete="off"
                      className="form-control"
                      name="reEnterPassword"
                      onChange={this.handleChange}
                      type="text"
                      value={reEnterPassword}
                    />
                    <div style={{ fontSize: 12, color: "red" }}>
                      {this.state.reEnterPasswordError}
                    </div>
                  </div>
                ) : null}
                <button
                  type="submit"
                  className="btn btn-block btn-lg"
                  style={{
                    backgroundColor: "black",
                    color: "white",
                    marginTop: 20
                  }}
                >
                  {buttonText}
                </button>
              </form>
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  marginTop: 10
                }}
              >
                {buttonText === "Signup" ? (
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <small>already have an account!</small>
                    <Link
                      to="/signin"
                      className="nav-link"
                      style={{ color: "red" }}
                    >
                      Log in
                    </Link>
                  </div>
                ) : (
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <small>New to KAPDAGRAM?</small>
                    <Link
                      to="/signup"
                      className="nav-link"
                      style={{ color: "red" }}
                    >
                      Create an account.
                    </Link>
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className="col-md-3 col-lg-4" />
        </div>
      </div>
    );
  }
}

export default AuthForm;
