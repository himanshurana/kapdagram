import React, { Component } from "react";
import "../CSS/landing.css";
import { connect } from "react-redux";
import { SimpleSlider, EditorsChoice } from "../containers/Slider";

class Homepage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      staffPick: [],
      departments: [
        {
          title: "NO WAY OUT T-SHIRT",
          price: "499",
          img:
            "https://cdn.shopify.com/s/files/1/0065/0788/8755/products/no_way_out_black_360x.png?v=1538949369"
        }
        //   {
        //     title: "NO WAY OUT T-SHIRT",
        //     price: "499",
        //     img:
        //       "https://cdn.shopify.com/s/files/1/0065/0788/8755/products/unk_nown_blue_360x.png?v=1535282261"
        //   },
        //   {
        //     title: "HUMAN LONG SLEEVES T-SHIRT",
        //     price: "499",
        //     img:
        //       "https://cdn.shopify.com/s/files/1/0065/0788/8755/products/7a362df55f7ff37a48a54df1f2550f53_360x.jpg?v=1535281913"
        //   }
      ]
    };
  }

  componentWillReceiveProps(nextProps) {
    // this.setState({ staffPick: nextProps.staffPick });
    // load  display element block
    // let loader = document.querySelector(".loader-content");
    // // remove loader from screen
    // loader.style.display = "none";
  }

  renderFeaturedCollections = () => {
    return this.state.departments.map((department, i) => {
      return (
        <div key={i} className="col-md-4" style={{ marginTop: 25 }}>
          <img src={department.img} width="100%" height="auto" alt="img" />
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
              padding: "10px 0"
            }}
          >
            <p>{department.title}</p>
            <p>{department.price}</p>
          </div>
        </div>
      );
    });
  };
  6;
  render() {
    const { staffPick } = this.state;
    return (
      <div>
        <div>
          <div className="container">
            <SimpleSlider />
            <EditorsChoice />
          </div>
        </div>
        <div className="container">
          <div style={{ marginTop: 25 }}>
            <small>Featured collection</small>
            <div className="row" style={{ justifyContent: "center" }}>
              Free Shipping
              <img src="https://img.icons8.com/nolan/64/000000/free-shipping.png"></img>
            </div>
          </div>
          {/* <div style={{ margin: "50px 0", textAlign: "center" }}>
            <h4>S T A F F P I C K</h4>
            <div className="row">{this.renderItems(staffPick)}</div>
          </div> */}
          {/* <div style={{ marginTop: 25 }}>
            <small>Featured collection</small>
            <div className="row" style={{ justifyContent: "center" }}>
              {this.renderFeaturedCollections()}
            </div>
          </div> */}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ staffPick }) => {
  return { staffPick };
};

export default connect(mapStateToProps)(Homepage);
