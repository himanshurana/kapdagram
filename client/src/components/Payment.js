import React, { Component } from "react";
import { Link } from "react-router-dom";
import { addPaymentModeToCheckoutObect } from "../store/actions/checkoutActions";
import { postOrder } from "../store/actions/orderActions";
import { connect } from "react-redux";

class Payment extends Component {
  render() {
    const payment = { method: "COD" };
    return (
      <div>
        <input
          type="radio"
          id="huey"
          name="drone"
          value="huey"
          onChange={() => this.props.addPaymentModeToCheckoutObect(payment)}
        />
        <label>COD</label>

        <Link to="/orders">
          <button
            onClick={() => {
              this.props.postOrder(
                this.props.currentUser.user.id,
                this.props.checkoutObject
              );
            }}
            className="btn btn-primary"
          >
            CONFIRM ORDER
          </button>
        </Link>
      </div>
    );
  }
}
const mapStateToProps = ({ checkoutObject, currentUser }) => {
  return { checkoutObject, currentUser };
};

export default connect(
  mapStateToProps,
  { addPaymentModeToCheckoutObect, postOrder }
)(Payment);
