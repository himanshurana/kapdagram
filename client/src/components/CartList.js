import React from "react";
import "../CSS/cartList.css";
const deleteLogo = require("../assests/icons/delete-button.png");
const subtractBtn = require("../assests/icons/substract.png");
const addBtn = require("../assests/icons/add.png");

const increaseItem = (addToCart, item, size) => {
  item.product.selectedVariant.selectedSize = size;
  addToCart(item.product);
  console.log(item.product.selectedVariant, size);
};

const removeItem = (onRemove, size, variant_id) => {
  let data = { variant_id, updateSize: size };
  onRemove(data);
};

export default ({ cart, reduce, addToCart, onRemove }) => {
  return cart.products.map((item, i) => {
    return item.selectedVariant.selectedSize.map((size, i) => {
      return (
        <div className="row cart-list-item-container" key={i}>
          <div className="col-4 col-sm-2">
            <div className="cart-list-item-img-container">
              <img
                src={item.selectedVariant.variant.assests.imgs[0]}
                alt="product"
                height="auto"
                width="100%"
              />
              
            </div>
          </div>
          <div className="col-8 col-sm-10">
            <div className="cart-list-item-right-side-container">
              {/* <div className="col col-sm-8"> */}
                {/* <div > */}
                  <p>{item.product.title} ROUND NEXK T-SHIRT</p>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <small style={{ color: "gray" }}>size</small>
                    <h6>{size.size}</h6>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <small style={{ color: "gray" }}>color</small>
                    <h6>{item.selectedVariant.variant.attributes.color}</h6>
                  </div>
                  <div
                    
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <small style={{ color: "gray" }}>amount</small>
                    <h6>{size.price}</h6>
                  </div>
                {/* </div> */}
              {/* </div>
              <div className="col col-sm-4" style={{ display: "flex" }}> */}
                <div className="btn-group cart-list-item-btns-container">
                  {size.qty === 1 ? (
                    <img
                      src={deleteLogo}
                      alt="delete button icon"
                      height="16"
                      width="16"
                      style={{ cursor: "pointer" }}
                      onClick={() => {
                        removeItem(
                          onRemove,
                          size.size,
                          item.selectedVariant.variant._id
                        );
                      }}
                    />
                  ) : (
                    <img
                      src={subtractBtn}
                      alt="decrease button icon"
                      height="16"
                      width="16"
                      style={{ cursor: "pointer" }}
                      onClick={() => {
                        console.log(item.product);
                        reduce(item.product);
                      }}
                    />
                  )}

                  <div>{size.qty}</div>
                  <img
                    src={addBtn}
                    alt="increase button icon"
                    height="16"
                    width="16"
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      increaseItem(addToCart, item, size.size);
                    }}
                  />
                </div>
              {/* </div> */}
            </div>
          </div>
        </div>
      );
    });
    // return (
    //   <div
    //   className="row"
    //     key={i}
    //     style={{
    //       boxShadow: "2px 2px 0.5px whitesmoke",
    //       border: "2px solid whitesmoke",
    //       margin: "5px 0 0 0",
    //       padding: "10px 0",
    //       display: "flex",
    //       alignItems: "center"
    //     }}
    //   >
    //     <div className="col col-sm-2">
    //       <div
    //         style={{
    //           display: "flex",
    //           flexDirection: "column",
    //           justifyContent: "center",
    //           alignItems: "center"
    //         }}
    //       >
    //         <img
    //           src={item.selectedVariant.variant.assests.imgs[0]}
    //           alt="product"
    //           height="auto"
    //           width="50%"
    //         />
    //         <p>{item.product.title}</p>
    //       </div>
    //     </div>
    //     <div className="col col-sm-10">
    //       <div className="row">
    //         <div className="col col-sm-8">
    //           <div className="row">
    //             <div
    //               className="col"
    //               style={{
    //                 display: "flex",
    //                 flexDirection: "column",
    //                 justifyContent: "center",
    //                 alignItems: "center"
    //               }}
    //             >
    //               <small style={{ color: "gray" }}>size</small>
    //               <h6>{item.selectedVariant.selectedSize}</h6>
    //             </div>
    //             <div
    //               className="col"
    //               style={{
    //                 display: "flex",
    //                 flexDirection: "column",
    //                 justifyContent: "center",
    //                 alignItems: "center"
    //               }}
    //             >
    //               <small style={{ color: "gray" }}>color</small>
    //               <h6>{item.selectedVariant.variant.attributes.color}</h6>
    //             </div>
    //             <div
    //               className="col"
    //               style={{
    //                 display: "flex",
    //                 flexDirection: "column",
    //                 justifyContent: "center",
    //                 alignItems: "center"
    //               }}
    //             >
    //               <small style={{ color: "gray" }}>amount</small>
    //               <h6>{item.price}</h6>
    //             </div>
    //           </div>
    //         </div>
    //         <div className="col col-sm-4" style={{display: "flex"}}>
    //           <div className="row">
    //             <div className="col-8 "
    //               style={{
    //                 display: "flex",
    //                 justifyContent: "center",
    //                 alignItems: "center"
    //               }}
    //             >
    //               <div
    //               className="btn-group"
    //                 style={{
    //                   width: "80px",
    //                   justifyContent: "space-between"
    //                 }}
    //               >
    //                 <img
    //                   src={subtractBtn}
    //                   alt="decrease button icon"
    //                   height="24"
    //                   width="24"
    //                   style={{ cursor: "pointer" }}
    //                   onClick={() => {
    //                     reduce(item.product._id, item.selectedVariant.variant._id);
    //                   }}
    //                 />
    //                 <div>
    //                   {item.qty}
    //                 </div>
    //                 <img
    //                   src={addBtn}
    //                   alt="increase button icon"
    //                   height="24"
    //                   width="24"
    //                   style={{ cursor: "pointer" }}
    //                   onClick={() => {
    //                     addToCart(item.product);
    //                   }}
    //                 />
    //                 <span />
    //               </div>
    //             </div>
    //             <div className="col-4 "
    //               style={{
    //                 display: "flex",
    //                 flexDirection: "column",
    //                 justifyContent: "center",
    //                 alignItems: "center"
    //               }}
    //             >
    //               <img
    //                 src={deleteLogo}
    //                 alt="delete button icon"
    //                 height="24"
    //                 width="24"
    //                 style={{ cursor: "pointer" }}
    //                 onClick={() => {
    //                   onRemove(item.selectedVariant.variant._id);
    //                 }}
    //               />
    //             </div>
    //           </div>
    //         </div>
    //       </div>
    //     </div>
    //   </div>
    // );
  });
};
