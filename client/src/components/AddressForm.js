import React, { Component } from "react";
import { connect } from "react-redux";
import { findDOMNode } from "react-dom";
import { postAddress } from "../store/actions/addressActions";

class AddressForm extends Component {
  handleSubmit(event) {
    event.preventDefault();
    let history = this.props.history;
    const address = {
      mobile: findDOMNode(this.refs.mobile).value,
      email: this.props.currentUser.user.email,
      // email: findDOMNode(this.refs.email).value,
      customer_name: findDOMNode(this.refs.name).value,
      customer_address_first_line: findDOMNode(this.refs.address_first_line)
        .value,
      customer_address_second_line: findDOMNode(this.refs.address_second_line)
        .value,
      landmark: findDOMNode(this.refs.landmark).value,
      city: findDOMNode(this.refs.city).value,
      state: findDOMNode(this.refs.state).value,
      pincode: findDOMNode(this.refs.pincode).value
    };
    const user_Id = this.props.currentUser.user.id;
    this.props.postAddress(user_Id, address, history);
  }

  render() {
    return (
      <div className="row" style={{ margin: "25px 0" }}>
        <div className=" col-md-2 col-lg-3" />
        <div className="col-md-8 col-lg-6">
          <form onSubmit={this.handleSubmit.bind(this)}>
            <div className="form-row">
              <div className="form-group col-md-6">
                <label >Name *</label>
                <input
                  ref="name"
                  type="text"
                  className="form-control"
                  placeholder="name"
                  required
                />
              </div>
              <div className="form-group col-md-6">
                <label>mobile *</label>
                <input
                  ref="mobile"
                  type="number"
                  className="form-control"
                  placeholder="mobile"
                  required
                />
              </div>
            </div>
            <div className="form-group">
              <label >Address *</label>
              <input
                ref="address_first_line"
                type="text"
                className="form-control"
                placeholder="1234 Main St"
                required
              />
            </div>
            <div className="form-group">
              <label >Address 2 *</label>
              <input
                ref="address_second_line"
                type="text"
                className="form-control"
                placeholder="Apartment, studio, or floor"
                required
              />
            </div>
            
            <div className="form-group">
                <label>Landmark *</label>
                <input
                  ref="landmark"
                  type="text"
                  className="form-control"
                  placeholder="Near SBI Bank"
                  required
                />
              </div>
            <div className="form-group" />
            <div className="form-row">
              <div className="form-group col-md-6">
                <label>City *</label>
                <input
                  ref="city"
                  type="text"
                  className="form-control"
                  placeholder="Near SBI Bank"
                  required
                />
              </div>
              <div className="form-group col-md-4">
                <label>State *</label>
                <input
                  type="text"
                  className="form-control"
                  ref="state"
                  required
                />
              </div>
              <div className="form-group col-md-2">
                <label>Zip *</label>
                <input
                  type="text"
                  className="form-control"
                  ref="pincode"
                  required
                />
              </div>
            </div>
            {/* <div className="form-group">
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="checkbox"
                  id="gridCheck"
                />
                <label className="form-check-label " for="gridCheck">
                  Check me out
                </label>
              </div>
            </div> */}
            {/* <Link to="/checkout_address"> */}
              <button type="submit" className="btn btn-primary form-control" style={{backgroundColor: "black", color: "white"}}>
                Add Address
              </button>
            {/* </Link> */}
          </form>
        </div>
        <div className="ol-ms-2 col-lg-3" />
      </div>
    );
  }
}

const mapStateToProps = ({ currentUser }) => {
  return { currentUser };
};

export default connect(
  mapStateToProps,
  { postAddress }
)(AddressForm);