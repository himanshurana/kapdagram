import React from "react";

const CategoriesList = ({ categories, filterCategories }) => {
  return categories.map(category => {
    return (
      <p
        onClick={() => filterCategories(category)}
        className="category"
        style={{
          fontFamily: "Port Lligat Sans",
          fontWeight: "bold",
          color: "black"
        }}
      >
        {category.title}
      </p>
    );
  });
};

export default CategoriesList;