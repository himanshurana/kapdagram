import React, { Component } from "react";
class PrivacyPolicy extends Component {
  render() {
    return (
      <div className="container">
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            marginTop: 25
          }}
        >
          <div style={{ fontSize: 20, fontWeight: "bold", margin: "25px 0" }}>
            Refund and Cancellation Policy
          </div>
          <div>
            <p>
              Our focus is complete customer satisfaction. In the event, if you
              are displeased with the services provided, we will refund back the
              money, provided the reasons are genuine and proved after
              investigation. Please read the fine prints of each deal before
              buying it, it provides all the details about the services or the
              product you purchase.
            </p>
            <p>
              In case of dissatisfaction from our services, clients have the
              liberty to cancel their projects and request a refund from us. Our
              Policy for the cancellation and refund will be as follows:
            </p>
            <div style={{ fontWeight: "bold" }}>Cancellation Policy</div>
            <p>For Cancellations please contact the us via contact us link.</p>
            <p>
              Requests received later than ____business days prior to the end of
              the current service period will be treated as cancellation of
              services for the next service period.
            </p>
            <div style={{ fontWeight: "bold" }}>Refund Policy</div>
            <p>
              We will try our best to create the suitable design concepts for
              our clients.
            </p>
            <p>
              In case any client is not completely satisfied with our products
              we can provide a refund.
            </p>
            <p>
              If paid by credit card, refunds will be issued to the original
              credit card provided at the time of purchase and in case of
              payment gateway name payments refund will be made to the same
              account.
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default PrivacyPolicy;
