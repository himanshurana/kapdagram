import React from "react";
import removeBtn from "../assests/icons/cancel.png";

const AddressListItem = ({
  history,
  currentUser,
  addressArr,
  removeAddress,
  addAddressToCheckoutObect
}) => {
  if (addressArr.length > 0) {
    return addressArr.map((address, index) => {
      return (
        <div className="col-sm-6 col-lg-4" key={index}>
          <div
            style={{
              position: "relative",
              border: "0.5px solid whitesmoke",
              boxShadow: "2.5px 2.5px 0.5px whitesmoke",
              padding: 10
            }}
          >
            <div
              onClick={() => {
                addAddressToCheckoutObect(address, history);
              }}
              // to={"/checkout_summary"}
              style={{
                color: "black",
                textDecoration: "none"
              }}
            >
              <div  style={{fontWeight:"bold"}}>{address.customer_name}</div>
              <div>{address.email}</div>
              <div>{address.mobile}</div>
              <div>{address.customer_address_first_line}</div>
              <div>{address.customer_address_second_line}</div>
              <div>{address.landmark}</div>
              <div>{address.pincode}</div>
            </div>
            <div
              style={{
                position: "absolute",
                right: 10,
                top: 10,
                cursor: "pointer"
              }}
              onClick={() => removeAddress(currentUser.user.id, address._id)}
            >
              <img src={removeBtn} width="12" height="12" alt="address remove btn img" />
            </div>
          </div>
        </div>
      );
    });
  }
  return <p>Empty AddressArr!!</p>;
};

export default AddressListItem;
