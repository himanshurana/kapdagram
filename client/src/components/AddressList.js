import React from "react";
// COMPONENT
import AddressListItem from "./AddressListItem"; //Actions
// CSS
// import '../CSS/productsPage.css'

const AddressList = ({
  history,
  currentUser,
  addressArr,
  removeAddress,
  addAddressToCheckoutObect
}) => {
  return (
    <div>
      <p style={{ borderBottom: "1px solid lightgray", color: "gray" }}>
        Select Address
      </p>
      <div className="row">
        <AddressListItem
          history={history}
          currentUser={currentUser}
          addressArr={addressArr}
          removeAddress={removeAddress}
          addAddressToCheckoutObect={addAddressToCheckoutObect}
        />
      </div>
    </div>
  );
};

export default AddressList;
