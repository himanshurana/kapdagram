import React, { Component } from "react";
class ContectUs extends Component {
  render() {
    return (
      <div
        style={{
          display: "flex",
          flexDirection:"column",
          justifyContent: "center",
          alignItems: "center",
          height: "82vh"
        }}
      >
        <div style={{fontSize: "20px", fontWeight: "bold"}}>Address</div>
        <div>487, 30, Peera Garhi,</div>
        <div> Peeragarhi Village, Paschim Vihar, </div>
        <div>New Delhi, Delhi 110087</div>
        <div style={{ fontWeight: "bold"}}>Mobile: 8802869692</div>
      </div>
    );
  }
}

export default ContectUs;
