import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getProduct } from "../store/actions/products";
import "../CSS/productsListItem.css";

const ProductsListItem = ({ fetchedProducts }) => {
  let products = fetchedProducts;
  if (products.length) {
    return products.map(product => {
      return product.variants.map((variant, i) => {
        return (
          <Link
            key={i}
            className="col-6 col-lg-2 productListItemCard"
            to={`/products/${product._id}/variant/${variant._id}`}
            style={{ color: "black", textDecoration: "none" }}
          >
            <div style={{ margin: "10px 0" }}>
              <img
                src={variant.assests.imgs[0]}
                alt="product"
                style={{ width: "100%", height: "auto" }}
              />
             
              <div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
              <div className="product-list-title">{product.title}</div>
                <div>
                  <span className="product-price-text">
                    ₹{product.prices.sell_price}
                  </span>
                </div>
                {/* <div
                style={{
                  marginLeft: 5,
                  fontSize: "14px",
                  position: "relative"
                }}
              >
                <div>{product.prices.mrp}</div>
                <div
                  style={{
                    borderBottom: "2px solid red",
                    position: "absolute",
                    top: 10,
                    width: 22
                  }}
                />
              </div>
              <div
                style={{ fontSize: "13px", marginLeft: 5, color: "#388e3c" }}
              >
                <div>{product.prices.discount}% off</div>
              </div> */}
              </div>
            </div>
          </Link>
        );
      });
    });
  } else {
    return (
      <div id="empty-products-container">
        <div id="empty-products-title">coming soon!</div>
      </div>
    );
  }
};

export default connect(
  null,
  { getProduct }
)(ProductsListItem);
