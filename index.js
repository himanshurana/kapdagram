require("dotenv").config();
var express = require("express");
var app = express();
const cors = require("cors");
var mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");
const session = require("express-session");
const MongoStore = require("connect-mongo")(session);

require("./models")
const errorHandler = require("./handlers/error");
const authRoutes = require("./routes/auth");

// comment this api after run it one time so that no one can run it again and create the other counter model
// app.get("/counter", async(req, res)=>{
//   await counter.create({totalOrdersQuantity: 0});
// })

// Delhivery routes
const delhiveryRoutes = require("./routes/delhivery");

// Paytm route file
let paytmRoutes = require("./routes/paytm");

// Node routes
// Admin routes

const adminAuthRoutes = require("./routes/admin/auth");
const adminDepartmentRoutes = require("./routes/admin/departmentRoutes");
const adminCategoriesRoutes = require("./routes/admin/categoriesRoutes");
const adminGenreRoutes = require("./routes/admin/genreRoutes");
const adminVariantRoutes = require("./routes/admin/variantRoutes");
const adminOrderRoutes = require("./routes/admin/orderRoutes");
const adminProductsRoutes = require("./routes/admin/products");

// shared routes
const departmentRoutes = require("./routes/departmentRoutes");
const categoriesRoutes = require("./routes/categoriesRoutes");
const genreRoutes = require("./routes/genreRoutes");
const productsRoutes = require("./routes/products");
const ReviewRoutes = require("./routes/reviewRoutes");
const addressesRoutes = require("./routes/address");
const cartRoutes = require("./routes/cart");
const { loginRequired, ensureCorrectUser } = require("./middleware/auth");
const orderRoutes = require("./routes/orderRoutes");
const keys = require("./config/keys");
const db = require("./models");
const PORT = 8589;
var Cart = require("./models/cart");

// const{createPackage} = require("./handlers/delhivery")
// createPackage();;
require("./services/passport");

app.use(bodyParser.json()).use(bodyParser.urlencoded({ extended: true }));
app.use(cors({ credentials: true, origin: "http://localhost:3000" }));

// --->>> SET UP SESSIONS <<<---
var database = mongoose.connection;
database.on(
  "error",
  console.error.bind(console, "# MongoDB - connection error: ")
);
app.use(
  session({
    secret: "mySecretString",
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge: 1000 * 60 * 60 * 24 * 2 }, // 2 days in milliseconds
    store: new MongoStore({
      mongooseConnection: database,
      ttl: 2 * 24 * 60 * 60
    })
    //ttl: 2 days * 24 hours * 60 minutes * 60 seconds
  })
);
//--->>> END SESSION SET UP <<<---

// tell passport. it should use cookies to handle authentication
app.use(passport.initialize());
app.use(passport.session());

app.use("/", authRoutes, paytmRoutes, delhiveryRoutes);

app.use(
  "/api",
  departmentRoutes,
  categoriesRoutes,
  genreRoutes,
  productsRoutes,
  cartRoutes,
  
);

app.use(
  "/api/admin/users/:id",
  adminAuthRoutes,
  // loginRequired,
  // ensureCorrectUser,
  adminDepartmentRoutes,
  adminCategoriesRoutes,
  adminGenreRoutes,
  adminVariantRoutes,
  adminOrderRoutes,
  adminProductsRoutes
  // orderRoutes
);

app.use(
  "/api/users/:id",
  loginRequired,
  ensureCorrectUser,
  ReviewRoutes,
  addressesRoutes,
  orderRoutes
);

app.use("/uploads", express.static("uploads"));

app.get("/api/reviews", async function(req, res, next) {
  try {
    let reviews = await db.Review.find()
      .sort({ createdAt: "desc" })
      .populate("user", {
        displayName: true
      });
    console.log(reviews);
    return res.status(200).json(reviews);
  } catch (err) {
    return next(err);
  }
});

//////////////////////////////////////////////////////////
app.use(function(req, res, next) {
  let err = new Error("Not Found");
  err.status = 404;
  next(err);
});

app.use(errorHandler);

app.listen(PORT, function() {
  console.log(`Server is starting on port ${PORT}`);
});

// // --->>> GET BOOKS IMAGES API <<<-----
// app.get('/images', function (req, res) {
//   const imgFolder = __dirname + '/public/images/';
//   // REQUIRE FILE SYSTEM
//   const fs = require('fs');
//   //READ ALL FILES IN THE DIRECTORY
//   fs.readdir(imgFolder, function (err, files) {
//     if (err) {
//       return console.error(err);
//     }
//     //CREATE AN EMPTY ARRAY
//     const filesArr = [];
//     // ITERATE ALL IMAGES IN THE DIRECTORY AND ADD TO THE THE ARRAY
//     files.forEach(function (file) {
//       filesArr.push({ name: file });
//     });
//     // SEND THE JSON RESPONSE WITH THE ARARY
//     res.json(filesArr);
//   })
// })
// // END APIs
