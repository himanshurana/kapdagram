const express = require("express");
const router = express.Router({ mergeParams: true }); //And here we need to pass in an option called merge perhaps and merge Parama allows us to get access
//to the ID inside of this router. "/:id"

const {
  createOrder,
  getOrders,
  cancelOrder,
  trackOrder
} = require("../handlers/orders");

// prefix - /api/users/:id/
router
  .route("/orders")
  .post(createOrder)
  .get(getOrders);

// prefix - /api/users/:id/
router
  .route("/orders/:order_id")
  .put(cancelOrder)
  .get(trackOrder)
  .get(getOrders)
  .post(createOrder);

module.exports = router;
