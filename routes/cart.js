const express = require("express");
const router = express.Router({ mergeParams: true });

const { createCart, reduce, getCart, deleteCart } = require("../handlers/Cart");

router.post("/add-to-cart", createCart);
router.post("/cart/reduce_item", reduce);
router.get("/cart", getCart);
router.post("/cart/remove_item", deleteCart);
module.exports = router;
