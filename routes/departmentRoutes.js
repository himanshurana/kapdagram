const express = require("express");
const router = express.Router({ mergeParams: true });

const { getDepartments } = require("../handlers/departments");

router.get("/departments", getDepartments);

module.exports = router;
