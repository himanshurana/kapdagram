const express = require("express");
const router = express.Router({ mergeParams: true });

const {
  createAddress,
  getAddress,
  deleteAddress
} = require("../handlers/address");

// prefix - /api/users/:id
router
  .route("/address")
  .get(getAddress)
  .post(createAddress);

// prefix - /api/users/:id
router.route("/address/:id/").delete(deleteAddress);

module.exports = router;
