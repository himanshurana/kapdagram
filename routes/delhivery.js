const express = require("express");
const router = express.Router({ mergeParams: true });

const {
  checkPincode
} = require("../handlers/delhivery");

router.get("/check-pincode/:pincode", checkPincode);

module.exports = router;
