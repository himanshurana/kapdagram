const express = require("express");
const router = express.Router({ mergeParams: true });

const {
  getGenres
} = require("../handlers/genres");

router.get("/genres", getGenres);

module.exports = router;
