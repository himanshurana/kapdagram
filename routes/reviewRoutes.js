const express = require("express");
const router = express.Router({ mergeParams: true }); //And here we need to pass in an option called merge perhaps and merge Parama allows us to get access
                                                      //to the ID inside of this router. "/:id"

const {
  creatReview,
  getReview,
  deleteReview
} = require("../handlers/reviews");

// prefix - /api/users/:id/
router.route("/reviews").post(creatReview);

// prefix - /api/users/:id/
router
  .route("reviews/:review_id")
  .get(getReview)
  .delete(deleteReview);

module.exports = router;
