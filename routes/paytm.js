const express = require("express");
const router = express.Router({ mergeParams: true }); //And here we need to pass in an option called merge perhaps and merge Parama allows us to get access
                                                      //to the ID inside of this router. "/:id"
const {
    generateChecksum
} = require("../handlers/paytm");

// prefix - /api/users/:id/
router.route("/generate_checksum").post(generateChecksum);

module.exports = router;
