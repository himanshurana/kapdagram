const express = require("express");
const router = express.Router();
var passport = require("passport");
const {
  signup,
  signin,
  adminSignup,
  adminSignin
} = require("../handlers/auth");

// router.post("/api/admin/auth/signup", adminSignup);
// router.post("/api/admin/auth/signin", adminSignin);

router.post("/api/auth/signup", signup);
router.post("/api/auth/signin", signin);

router.get(
  "/auth/google",
  passport.authenticate("google", {
    scope: ["profile", "email"]
  })
);

router.get(
  "/auth/google/callback",
  passport.authenticate("google"),
  (req, res) => {
    res.send(req.user);
    // res.redirect('http://localhost:3000/')
  }
);

router.get("/api/logout", (req, res) => {
  req.logout();
  res.send(req.user);
});

router.get("/api/current_user", (req, res) => {
  // res.redirect('http://localhost:3000/')
  res.send(req.user);
});

module.exports = router;
