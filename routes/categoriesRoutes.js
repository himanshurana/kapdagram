const express = require("express");
const router = express.Router({ mergeParams: true });

const { getCategories } = require("../handlers/categories");
router.route("/categories").get(getCategories);

module.exports = router;
