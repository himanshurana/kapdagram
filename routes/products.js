const express = require("express");
const router = express.Router({ mergeParams: true });
const callbacks = require("../handlers/products");
const multer = require("multer");

// prefix - /api
router.route("/products").get(callbacks.getProducts);

// prefix - /api
router.route("/products/:id").get(callbacks.getProduct);

module.exports = router;
