const express = require("express");
const router = express.Router({ mergeParams: true });

const { createGenre, updateGenre, deleteGenre } = require("../../handlers/admin/genres");

router.post("/genre", createGenre);

router
  .route("/genre/:id")
  .delete(deleteGenre)
  .put(updateGenre);

module.exports = router;
