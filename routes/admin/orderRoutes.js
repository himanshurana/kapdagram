const express = require("express");
const router = express.Router({ mergeParams: true }); //And here we need to pass in an option called merge perhaps and merge Parama allows us to get access
//to the ID inside of this router. "/:id"

const {
  // cancelOrder,
  getOrders,
  updateOrderStatus
} = require("../../handlers/admin/orders");

// prefix - /api/users/:id/
router.route("/orders").get(getOrders);

router
  .route("/orders/:order_id")
  // .delete(cancelOrder)
  .put(updateOrderStatus);

module.exports = router;
