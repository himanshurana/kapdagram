const express = require("express");
const router = express.Router();
const {
  signup,
  signin,
} = require("../../handlers/admin/auth");

router.post("/auth/signup", signup);
router.post("/auth/signin", signin);

router.get("/logout", (req, res) => {
  req.logout();
  res.send(req.user);
});

router.get("/current_user", (req, res) => {
  // res.redirect('http://localhost:3000/')
  res.send(req.user);
});

module.exports = router;
