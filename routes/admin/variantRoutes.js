const express = require("express");
const router = express.Router({ mergeParams: true });
const callbacks = require("../../handlers/admin/variant");
const multer = require("multer");

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./uploads/");
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString().replace(/:/g, "-") + file.originalname);
  }
});

const upload = multer({ storage: storage });

// prefix - /api
router
  .route("/variant")
  .post(upload.array("imgs", 10), callbacks.createVariant)

// prefix - /api
router
  .route("/variant/:variant_id")
  .put(upload.array("imgs", 10), callbacks.updateVariant)
  .delete(callbacks.deleteVariant)
module.exports = router;
