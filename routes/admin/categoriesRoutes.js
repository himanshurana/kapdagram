const express = require("express");
const router = express.Router({ mergeParams: true });

const {
  createCategory,
  updateCategory,
  deleteCategory
} = require("../../handlers/admin/categories");

router
  .route("/categories/:id")
  .delete(deleteCategory)
  .put(updateCategory);
router
  .route("/categories")
  .post(createCategory)

module.exports = router;
