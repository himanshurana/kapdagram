const express = require("express");
const router = express.Router({ mergeParams: true });

const {
  createDepartment,
  updateDepartment,
  deleteDepartment
} = require("../../handlers/admin/departments");

router.post("/department", createDepartment);

router
  .route("/department/:id")
  .delete(deleteDepartment)
  .put(updateDepartment);

module.exports = router;
