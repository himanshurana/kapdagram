const express = require("express");
const router = express.Router({ mergeParams: true });
const callbacks = require("../../handlers/admin/products");
const multer = require("multer");

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./uploads/");
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString().replace(/:/g, "-") + file.originalname);
  }
});

const upload = multer({ storage: storage });

// prefix - /api
router
  .route("/products")
  .post(upload.array("imgs", 10), callbacks.postProducts)

// prefix - /api
router
  .route("/products/:id")
  .delete(callbacks.deleteProducts)
  .put(callbacks.updateProducts);

module.exports = router;
